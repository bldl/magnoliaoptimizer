{- | This module handles the interfacing with
     the Z3 SMT solver. -}
module Z3 (getEquivalentDefs) where

import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Sequence as Q
import Control.Monad
import Control.Monad.State

import Data.Maybe
import Data.List
import System.Process
import GHC.IO.Handle

import Algorithm.DominatingFrontier
import Structure.AST
import Structure.CFG
import Structure.SSA
import Structure.DAG

import Z3.RewriteRules
import Z3.Output

type Queue = Q.Seq

enqueue :: Queue a -> [a] -> Queue a
enqueue s l = s Q.>< Q.fromList l

dequeue :: Queue a -> Queue a
dequeue = Q.drop 1

topqueue :: Queue a -> a
topqueue s = Q.index s 0

setOptions :: Z3 ()
setOptions = do
  putZ3 "(set-option :smt.mbqi true)"
  putZ3 "(set-option :smt.macro-finder true)"

getSorts :: SSA -> S.Set Type
getSorts ssa =
  getVarSorts ssa `S.union` getFunSorts ssa

getVarSorts :: SSA -> S.Set Type
getVarSorts =
  S.fromList 
  . map (varType  . origVar)
  . M.elems
  . cfgVarAnons

origFun :: SSAFA f -> Maybe f
origFun (SSAFun f) = Just f
origFun (PhiFun _) = Nothing

getFunSorts :: SSA -> S.Set Type
getFunSorts =
  S.fromList 
  . mapMaybe (fmap funType . origFun)
  . M.elems
  . cfgFunAnons

declareSorts :: SSA -> Z3 ()
declareSorts = putZ3 .
  concatMap (\s -> 
    case s of
      Predicate     -> ""
      (Type "Bool") -> ""
      (Type "Int")  -> ""
      (Type str)   -> "(declare-sort " ++ str ++ ")\n") . S.toList . getSorts

{- | The 'getEquivalentDefs' uses z3 to find 
     expressions that are equivalent modulo the axioms. -}
getEquivalentDefs :: [FunDef Var Fun] -> String -> SSA -> [Axiom] -> [RewriteSuggestion] -> IO Z3Output
getEquivalentDefs defs z3Command ssa axs suggests = runZ3 ssa z3Command $ do
  setOptions
  declareSorts ssa
  putFunDefs ssa
  putVarDefs ssa
  putFunAxioms ssa defs
  _ <- checkSat
  assertAxioms ssa axs
  _ <- checkSat
  findDefs ssa suggests

{- | Sends all Function definitions to Z3.-}
putFunDefs :: SSA -> Z3 ()
putFunDefs ssa = mapM_ putZ3 (mkFunDefs ssa)

{- | Sends all Variable definitions to Z3.-}
putVarDefs :: SSA -> Z3 ()
putVarDefs ssa = mapM_ putZ3 (mkVarDefs ssa)

{- | If a function f is defined to an expression e
 we assert the axiom forall x. f x = e, then 
 we let the Z3 macro finder take care of reducing this
 axiom in a sensible way.-}
putFunAxioms :: SSA -> [FunDef Var Fun] -> Z3()
putFunAxioms ssa defs = 
  mapM_ (assertAxiom ssa) (mkFunAxs defs)

{- | 'assertAxioms' ssa a asserts the expression axioms a of ssa
     to Z3. -}
assertAxioms :: SSA -> [Axiom] -> Z3 ()
assertAxioms ssa axs = 
  mapM_ (assertAxiom ssa) axs


-- | The State of the Z3 Monad.
data Z3State = AS{
  z3In        :: Handle,
  z3Out       :: Handle,
  z3Proc      :: ProcessHandle,
  equivs      :: [Equiv],
  assertResults  :: [(Expression,LinePointer,AssertState)],
  toBeAddedGlobal :: [(Var,Expression)],
  uniqueDefs  :: [Var],
  visited :: S.Set CFGPtr
  }

stateToOut :: SSA -> Z3State -> Z3Output
stateToOut ssa s = 
 z3Output ssa (equivs s) (assertResults s)
          

type Z3 = StateT Z3State IO

-- | z3Init gives an empty state for the z3 monad
z3Init :: String -> IO Z3State
z3Init z3Command = do
  (inHandle,outHandle,_,procHandle) <- runInteractiveCommand z3Command
  hSetBuffering inHandle NoBuffering
  hSetBuffering outHandle NoBuffering
  return (AS inHandle outHandle procHandle [] [] [] [] S.empty)

{- | runZ3 takes a Z3 computation and
 returns a list of expression proven to be equal
 during that computation. -}
runZ3 :: SSA -> String -> Z3 () -> IO Z3Output
runZ3 ssa z3command p = do
  as <- z3Init z3command
  (_,r) <- runStateT p as
  let z3p = z3Proc as
  terminateProcess z3p
  return (stateToOut ssa r)

-- | Send a string to Z3
putZ3 :: String -> Z3 ()
putZ3 str = do
  inh <- gets z3In
  --liftIO $ putStrLn str
  liftIO $ hPutStr inh str
  liftIO $ hPutStr inh "\n"

-- | Get one line from Z3
getLineZ3 :: Z3 String
getLineZ3 = do
  outh <- gets z3Out
  liftIO $ hGetLine outh

{- | Sends a command to push a context
 - to Z3. -}
pushZ3 :: Z3 ()
pushZ3 = putZ3 "(push)"

{- | Sends a command to pop a context
 - to Z3. -}
popZ3 :: Z3 ()
popZ3 = putZ3 "(pop)"



{- | checkSat returns the current state of the
    sat solver. -}
checkSat :: Z3 SATSTATE
checkSat = do
  putZ3 "(check-sat)"
  str <- getLineZ3
  case str of
    "sat" -> return SAT
    "unsat" -> return UNSAT
    _       -> return UNKNOWN


{- | defineFun f ps t b sends the command
 -  (define-fun f ps t b). -}
defineFun :: Fun -> String -> Type -> String -> String
defineFun f ps t b =
  unwords ["(define-fun",
    show f,
    ps,
    unType t,
    b,
    ")"]

{- | From a function definition f x = e we make the
 - corresponding axiom Forall x. f x = e -}
mkFunAxs :: [FunDef Var Fun] -> [Axiom]
mkFunAxs = map go 
 where
  vars = map (\(Param _ v _) -> v)
  vares =  map (\(Param _ v _) -> V v)
  go (FunDef (FunClause f ps _) e) =
    Forall (vars ps) (Eq (Apply f (vares ps)) e)
 
    
  
{- | Defines every function given by cfgFunAnons.
 -  depending on whether the function has a corresponding
 -  smt-symbol or not, the function is either declared
 -  as a macro or as an uninterpreted function. -}
mkFunDefs :: SSA -> [String]
mkFunDefs ssa = map goFun (M.assocs (cfgFunAnons ssa))
  where
    goFun (f,PhiFun v) =
      let (SSAVarAnon (ASTVarAnon False _ t ) _) = cfgVarAnons ssa M.! v in
      unwords 
        ["(declare-fun",
         show f,
         "(", unType t, unType t, ")", -- TODO: More than two branches cannot occur in our programs,
                                       -- but there is nothing enforcing this fact.
         unType t,
         ")"]
    goFun (f@Fun{},SSAFun (ASTFunAnon _ [] t (Just a))) = 
      defineFun f "()" t a
    goFun (f@Fun{},SSAFun (ASTFunAnon _ ps t (Just a))) = 
      defineFun f (paramDeclList ps) t ("(" ++ a ++ " " ++  unwords (paramDeclVars ps) ++ ")")
    goFun (f@Fun{},SSAFun (ASTFunAnon _ ps t Nothing)) = 
      unwords ["(declare-fun", show f, typeList ps,  unType t, ")"]
    goFun _ = ""

-- | mkDefinitions defines z3 variables for 
--  every ssa variable.
mkVarDefs :: SSA -> [String]
mkVarDefs ssa = 
  filter (not . null) (map goVar  (M.assocs (cfgVarAnons ssa)))
  where
    goVar (v,SSAVarAnon (ASTVarAnon False _ t ) _)  =
      "(declare-const " ++ show v ++ " " ++ unType t ++ ")"
    goVar (v,SSAVarAnon (ASTVarAnon True  _ t ) _) = 
      ";(declare-const " ++ show v ++ " " ++ unType t ++ ")"

{- | Translates a type into the corresponding type
 -   for Z3 -}
unType :: Type -> String
unType Predicate     = "Bool"
unType (Type s)      = s

{- | Writes out the Z3 list of types corresponding to the given Params.-}
typeList :: [AnonParam] -> String
typeList ps = "(" ++ unwords (map (unType . (\(AnonParam _ t) -> t)) ps) ++ ")"

{- | Gives a list of variable names for a list of AnonParams.
 -  For AnonParam we use the variablenames y0 ... yn.
 -  Note: This function does not check for name clashes.-}
paramDeclVars :: [AnonParam] -> [String]
paramDeclVars ps = zipWith (\_ i -> 'y': show i) ps [(1::Integer)..]

{- | Returns the Z3 list of parameters corresponding to
 - the list of AnonParams. -}
paramDeclList :: [AnonParam] -> String
paramDeclList ps =
  "(" 
  ++ unwords (zipWith
    (\(AnonParam _ t) v -> "(" ++ v ++ " " ++ unType t ++ ")")
    ps (paramDeclVars ps))
  ++ ")"

{- | Give the Z3 corresponding to the given axiom -}
showAxiom :: SSA -> Axiom -> String
showAxiom _ (Forall [] expr) = showExpr expr
showAxiom ssa (Forall vs expr) = "(forall (" ++ unwords (map mkArgument vs) ++ ") " ++ showExpr expr ++ ")"
  where
    mkArgument v = let (SSAVarAnon orig _) = varAnnot v ssa in
           "(" ++ show v ++ " " ++ unType (varType orig) ++ ")"

-- | Prints an Expression as a z3 string.
showExpr :: Expression -> String
showExpr Undefined = ""
showExpr (Eq a b) = "(= " ++ showExpr a ++ " " ++ showExpr b ++ ")"
showExpr (Ite a b c) = "(ite " ++ (unwords . map showExpr $ [a,b,c]) ++ ")"
showExpr (V v) = show v
showExpr (Apply f []) = show f
showExpr (Apply f exprs) = "(" ++ show f ++ " " ++ unwords (map showExpr exprs) ++ ")"

{- | Traverses the SSA and finds
      all definitions. If Z3 proves two definitions to be
      equal, the expression is not recomputed, but the variable
      is reused instead. -} 
findDefs :: SSA -> [RewriteSuggestion] ->  Z3 ()
findDefs ssa suggests = go (Q.singleton (cfgRoot ssa))
  where
    execConds = reachabilityCondition ssa
    go queue
     | Q.null queue = return ()
     | otherwise = do
        let r = topqueue queue
        vis <- gets visited
        unless (r `S.member` vis) $
          do
           modify (\s -> s{visited = S.insert r (visited s)})
           pushZ3
           assertExpr (execConds M.! r)
   
           let phis = fromMaybe [] (M.lookup r (cfgPhiFunctions ssa))
           forM_ (zip [0..] phis) (\(lno,PhiAssign v i vs) -> do
             let phiExpr = Apply (Fun i) (map V vs)
             uqs <- gets uniqueDefs
             forM_ (vs \\ uqs) ( \nv -> do
               b <- findM (isEqualZ3 (V nv)) (map V uqs)
               case b of
                 Nothing -> modify (\s -> s{uniqueDefs = nv : uniqueDefs s})
                 _       -> return ())
             uqis <- gets uniqueDefs
             b <- findM (isEqualZ3 phiExpr) (map V uqis)
             case b of
               Nothing -> do
                 modify (\s -> s{uniqueDefs = v : uniqueDefs s})
                 assertGlobalEqual (r,lno) v phiExpr
               (Just a) -> assertGlobalEqual (r,lno) v a)
   
           let assigns = statements r ssa
           forM_ (zip [length phis..] assigns) (\(lno,st) ->

             case st of
             (StAssume expr) -> assertExpr expr
             (StAssert expr) -> do
               status <- checkAssert expr
               modify (\s -> s{assertResults = (expr,(r,lno),status):assertResults s})

             (StAssign (Assign v expr)) -> do
               uqs <- gets uniqueDefs
               forM_ (newVars uqs expr) ( \nv -> do
                 b <- findM (isEqualZ3 (V nv)) (map V uqs)
                 case b of
                   Nothing -> modify (\s -> s{uniqueDefs = nv : uniqueDefs s})
                   _       -> return ())
               uqis <- gets uniqueDefs
               b <- findM (isEqualZ3 expr) (map V uqis)
               case b of
                 Nothing -> do
                   modify (\s -> s{uniqueDefs = v : uniqueDefs s})
                   assertGlobalEqual (r,lno) v expr
                   let candidates = concatMap (instantiateSuggests (M.map origVar (cfgVarAnons ssa)) uqs) suggests
                   eqs <- filterM (isEqualZ3 expr) candidates
                   mapM_ (assertGlobalEqual (r,lno) v) eqs
                 (Just a) -> assertGlobalEqual (r,lno) v a)
           
           let cond = condition r ssa
           isFalseCond <- isUnsat cond
           isTrueCond <- isUnsat (notFun cond)
           when isTrueCond $
             addEquiv cond trueFun (r,length assigns)
           when isFalseCond $
             addEquiv cond falseFun (r,length assigns)
           popZ3
           addGlobalEqs
           go (dequeue queue `enqueue` children r ssa)

{- | returns the list of variables in the given expression,
 -  but not in the given list of variables.-}
newVars :: [Var] -> Expression -> [Var] 
newVars vs (V v) = [v | v `notElem` vs]
newVars vs (Apply _ es) = concatMap (newVars vs) es 
newVars vs (Ite e1 e2 e3) = newVars vs e1 ++ newVars vs e2 ++ newVars vs e3
newVars vs (Eq e1 e2) = newVars vs e1 ++ newVars vs e2
newVars _ Undefined = []

{- | Checks wether a given expression is satisfiable
 - by using Z3. -}
checkAssert :: Expression -> Z3 AssertState
checkAssert e = do
  pushZ3
  assertExpr e
  r <- checkSat
  popZ3
  case r of
    UNSAT -> return Contr
    UNKNOWN -> return Unknown
    SAT -> do
      pushZ3
      assertExpr (notFun e)
      r2 <- checkSat
      popZ3
      case r2 of
        UNSAT   -> assertExpr e >> return Taut
        SAT     -> assertExpr e >> return Assumed
        UNKNOWN -> return Unknown
        

{- | add an equivalence to the state -}
addEquiv :: Expression -> Expression -> LinePointer -> Z3 ()
addEquiv a b l =
  modify (\s -> s{equivs = Equiv a b l : equivs s})

{- | Schedules the given var to be asserted equal to the given expression
 - globally and adds this equivalence to the state. -}
assertGlobalEqual :: LinePointer -> Var -> Expression -> Z3 ()
assertGlobalEqual l a b = do
  assertExpr (Eq (V a) b)
  modify (\s -> s{toBeAddedGlobal = (a,b):toBeAddedGlobal s})
  addEquiv (V a) b l

{- | Asserts all equalities that were scheduled to be
 - added globally. NB: Does not check that we are
 - in the global context.-}
addGlobalEqs :: Z3 ()
addGlobalEqs = do
  toBeAdded <- gets toBeAddedGlobal
  mapM_ (\(a,b) -> assertExpr (Eq (V a) b)) toBeAdded 
  modify (\s -> s{toBeAddedGlobal = []})


{- | Returns the status of asserting a given expression.
 - This is done in its own context and does not affect
 - the satisfiability of the current state. -}
checkExpr :: Expression -> Z3 SATSTATE
checkExpr e = do
  pushZ3
  assertExpr e
  result <- checkSat
  popZ3
  return result

{- | Checks wether a given expression is unsatisfiable -}
isUnsat :: Expression -> Z3 Bool
isUnsat e = liftM (==UNSAT) (checkExpr e)

{- | isEqualZ3 a b returns True when Z3 is able 
     to prove that a is equal to b. -}
isEqualZ3 :: Expression -> Expression -> Z3 Bool
isEqualZ3 a b = isUnsat (notFun (Eq a b))


{- | findM p xs returns the first element x of
      xs where p x returns True. This is the monadic
      equivalent to 'Data.List.find'. -}
findM :: Monad m => (a -> m Bool) -> [a] -> m (Maybe a)
findM _ [] = return Nothing
findM p (x:xs) = do
  b <- p x
  if b
    then return (Just x)
    else findM p xs

{- | assertExpr e asserts the expression e
     to Z3. -}
assertExpr :: Expression -> Z3 ()
assertExpr expr = putZ3 ("(assert " ++ showExpr expr ++ ")")


{- | assert the given axiom in the current context -}
assertAxiom :: SSA -> Axiom -> Z3()
assertAxiom ssa axiom =  do
  let ax = "(assert " ++ showAxiom ssa axiom ++ ")"
  pushZ3
  putZ3 ax
  res <- checkSat
  popZ3
  case res of
    UNSAT   -> liftIO (putStrLn ("ERROR: Inconsistent axiom " ++ ax))
    SAT     -> putZ3 ax
    UNKNOWN -> liftIO (putStrLn ("Warning: Could not use axiom " ++ ax))
  
