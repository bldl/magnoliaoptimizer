{- | This module provides the function `printDot` which takes a SSA together with a set
   of equivalent expressions and returns a dot file string which can be used with graphviz
   to provide a graphical representation of the input. -}
module DOT(printDot) where

import qualified Data.Map as M
import Data.List
import Text.Html(toHtml,tr,td,concatHtml)

import Structure.CFG
import Structure.AST
import Structure.SSA
import Structure.DAG

import Z3.Output

{- | printDot returns a graphical representation of the given ssa together with some
   equivalent expressions as a dot file. -}
printDot :: Z3Output -> String
printDot out = 
  "digraph G {\nnode [shape = record,ordering=out];\n" ++ makeNodes (outSsa out) ++ "\n" ++ drawLines (outSsa out) ++ "\n}"
  ++ "\ndigraph {\n rank = sink; Legend [shape=none, margin=0, label =<"
  ++ "\n<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" CELLPADDING=\"4\">\n"
  ++ showLegendTables out
  ++ "</TABLE>\n>];\n}"

{- | Prints the lengend table for the DOT Diagram associated with the given
 - Z3Output. -}
showLegendTables :: Z3Output -> String
showLegendTables out = show . concatHtml . map go . legends (z3Equivs out) . outSsa $ out
  where
   go (key,value,loc) =
     tr (concatHtml [td (toHtml key), td (toHtml value), td (toHtml loc)])

{- | Gives a list of triples of DOT strings representing an
 - the Legend in the corresponding SSA together with the legends 
 - corresponding to the list of equivs.-}
legends :: [Equiv] -> SSA -> [(String,String,String)]
legends equivs ssa = 
  (map (fstSndOnly . funLegends) . M.assocs . cfgFunAnons $ ssa) ++
  (map (fstSndOnly . varLegends) . M.assocs . cfgVarAnons $ ssa) ++
  map equivLegends equivs
  where
    fstSndOnly (a,b) = (a,b,"")
    varLegends (v,SSAVarAnon va _) = (prettyV ssa v, show va)
    funLegends (f,fa) = (prettyF ssa f, show fa)
    equivLegends (Equiv e1 e2 loc) = (prettyE ssa e1, prettyE ssa e2,show loc)

{- | Pretty prints an variable. -}
prettyV :: SSA -> Var -> String
prettyV ssa (Var v) =
  let (SSAVarAnon ov _) = varAnnot (Var v) ssa in
    varName ov ++ "_" ++ show v

{- | Pretty prints a function. -}
prettyF :: SSA -> Fun -> String
prettyF ssa (Fun f) =
  let (SSAFun origF) = funAnnot (Fun f) ssa in
    funName origF
prettyF _ f = show f

{- | Pretty prints an Expression. -}
prettyE :: SSA -> Expression -> String
prettyE ssa (V v) = prettyV ssa v
prettyE ssa (Ite a b c) = "ite(" ++ intercalate "," (map (prettyE ssa) [a,b,c]) ++ ")"
prettyE ssa (Eq a b) = "_==_(" ++ prettyE ssa a ++ "," ++ prettyE ssa b ++ ")"
prettyE ssa (Apply f exprs) =
  prettyF ssa f ++ "(" ++ intercalate "," (map (prettyE ssa) exprs) ++ ")"
prettyE _ Undefined = "undefined"

{- | Gives a string, representing a list of DOT-lines 
 - corresponding to the lines in the CFG of the given
 - SSA. -}
drawLines :: SSA -> String
drawLines ssa =
  concatMap (\n -> 
    concatMap (\m -> concat ["\"node", show n, "\" -> \"node", show m,"\";\n"]) (children n ssa)) . vertecies $ ssa

{- | Gives a string, representing a list of DOT-nodes 
 - corresponding to the nodes in the CFG of the given
 - SSA. -}
makeNodes :: SSA -> String
makeNodes ssa =
  concatMap (\n -> concat ["node", show n, " [label =\"", printAssigs n ssa,"\"];\n"]) . vertecies $ ssa

{- | Prints the assignments done in the given node
 - in the given SSA.-}
printAssigs :: Vertex -> SSA -> String
printAssigs r ssa = 
  let phifuncs = phiFunctions r ssa in
  let cond = condition r ssa in
  "{" ++
  concatMap (\(PhiAssign v i vs) -> concat [prettyV ssa v, " := phi_", show i,"(", intercalate "," (map (prettyV ssa) vs),");\\n"]) phifuncs
  ++ "|" ++ (concatMap (\(Assign v expr) -> concat [prettyV ssa v, " := ",prettyE ssa expr,";\\n"]) . assignments r $ ssa)
  ++ "|" ++ (if cond /= trueFun && cond /= falseFun then prettyE ssa cond else "")
  ++ "}" 
