{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

{- | The Parser module defines a Parser for the Input language. -}
module Parser where
import qualified Text.Parsec.Token as P
import           Text.Parsec
import           Text.Parsec.String
import           Text.Parsec.Language(javaStyle)
import           Control.Monad

import           Structure.AST


-- | 'symbol' s parses on java style parse.
symbol :: String -> Parser String
symbol = P.symbol (P.makeTokenParser javaStyle)

{- | 'quotedString' parses one string enclosed in quotes. -}
quotedString :: Parser String
quotedString = do
  char '\"'
  x <- many (noneOf "\"")
  char '\"'
  return x

{- | brcks p parses "[<p>]". -}
brcks :: Parser a -> Parser a
brcks p = do
  symbol "["
  r <- p
  symbol "]"
  return r

{- | 'listOf' p parses a comma separated list of 
   elements parsed with p. Eg. listOf word matches
   \"[Spam,Egg,Bacon,Spam]\" and returns [\"Spam\",\"Egg\",\"Bacon\",\"Spam\"].-}
listOf :: Parser a -> Parser [a]
listOf p = do
  symbol "["
  xs <- p `sepBy` symbol ","
  symbol "]"
  return xs

{- | 'function0' n parses one function without any parameters.
      eg. function0 \"f\" matches \"f()\".  -}
function0 :: String -> Parser ()
function0 n = do
  symbol n
  symbol "("
  symbol ")"
  return ()

{- | 'function1' n p parses one function with one parameter
     parsed with p. eg.
     function 1 \"foo\" word matches \"foo(elephant)\".  -}
function1 :: String -> Parser b -> Parser b
function1 n p = do
  symbol n
  symbol "("
  f <- p
  symbol ")"
  return f

{- | 'function2' n p1 p2 c parses one function with two parameters
     parsed with p1 and p2. The result is combined with c. Eg.
     function2 \"bar\" word word (,) matches \"bar(elephant,turtle)\" and
     returns (\"elephant\",\"turtle\"). -}
function2 :: String -> Parser a -> Parser b -> (a -> b -> c) -> Parser c
function2 n p1 p2 c = do
  symbol n
  symbol "("
  f1 <- p1
  symbol ","
  f2 <- p2
  symbol ")"
  return (c f1 f2)

{- | 'function3' n p1 p2 p3 c parses one function with three parameters
     parsed with p1, p2 and p3. The result is combined with c. Eg.
     function2 \"bar\" word word word (,,) matches \"bar(spam,egg,bacon)\" and
     returns (\"spam\",\"egg\",\"bacon\"). -}
function3 :: String -> Parser a -> Parser b -> Parser c -> (a -> b -> c -> d) -> Parser d
function3 n p1 p2 p3 c = do
  symbol n
  symbol "("
  f1 <- p1
  symbol ","
  f2 <- p2
  symbol ","
  f3 <- p3
  symbol ")"
  return (c f1 f2 f3)

{- | 'function4' n p1 p2 p3 p4 c parses one function with three parameters
     parsed with p1, p2, p3 and p4. The result is combined with c. Eg.
     function2 \"bar\" word word word (,,,) matches \"bar(spam,spam,egg,bacon)\" and
     returns (\"spam\",\"spam\",\"egg\",\"bacon\"). -}
function4 :: String -> Parser a -> Parser b -> Parser c -> Parser d -> (a -> b -> c -> d -> e) -> Parser e
function4 n p1 p2 p3 p4 c = do
  symbol n
  symbol "("
  f1 <- p1
  symbol ","
  f2 <- p2
  symbol ","
  f3 <- p3
  symbol ","
  f4 <- p4
  symbol ")"
  return (c f1 f2 f3 f4)

{- | 'name' parses a string on the form \"Name(\"foo\")\". -}
name :: Parser String
name =  function1 "Name" quotedString

{- | 'typeIdent' parses a string on the form \"Type(\<'name'\>)\". -}
typeIdent :: Parser Type
typeIdent = liftM Type (function1 "TypeName" name) <|> (string "SpecialType(Predicate())" >> return Predicate)


{- | 'var' parses a string on the form
     \"Var(VarName(\<'name'\>,\<'typeIdent'\>))\". -}
var :: Parser ASTVarAnon
var = function1 "Var" 
       (function2 "VarName" name
        typeIdent (ASTVarAnon False))


{- | 'mode' matches \"Obs\", \"In\" and \"Out\". -}
mode :: Parser Mode
mode = 
  try (function0 "Obs" >> return Obs) <|>
  try (function0 "In"  >> return In) <|>
  try (function0 "Upd"  >> return Upd) <|>
  (function0 "Out" >> return Out)

{- | 'anonParam' matches a string of the form
     \"AnonParam(<'mode'>,\<'typeIdent'\>)\".-}
anonParam :: Parser AnonParam
anonParam = function2 "AnonParam" mode typeIdent AnonParam

{- | 'param' matches a string of the form
 -  \"param(\<'mode'\>,\<name\>,\<'typeIdent'\>)\".-}
param :: Parser (Param ASTVarAnon)
param =
 function3 "Param" mode name
   typeIdent (\m n t -> Param m (ASTVarAnon False n t) t)

{- | 'fun' matches
     \"Fun(\<'funName'\>)\". -}
fun :: Parser ASTFunAnon
fun =
  try (function2
    "Builtin"
    (function3 "FunName" 
    (function1 "QName" (listOf (function1 "Name" quotedString)))
    (listOf anonParam)
    typeIdent
    (,,))
    quotedString 
    (\(a,b,c) _ -> ASTFunAnon (head a) b c Nothing)) 
    <|> function1 "Fun" Parser.funName


{- | 'funName' matches
     \"FunName(\<'name'\>,\<'listOf' 'anonParam'\>,\<'typeIdent'\>)\". -}
funName :: Parser ASTFunAnon
funName = 
  function3 "FunName" name (listOf anonParam) typeIdent (\a b c -> ASTFunAnon a b c Nothing)

{- | 'apply' parses a string on the form 
     \"Apply(\<'fun'\>,\<'listOf' 'anonParam'\>)\",
     and returns (Apply f params).-}
apply :: Parser PreExpression
apply =
  choice . map try $
  [function2 "Apply" fun (listOf expr) Apply
  ,function2 "Equal" expr expr Eq
  ,function2 "Implies" expr expr (\a b -> orFun (notFun a) b)
  ,function3 "IfThenElseExpr" expr expr expr Ite
  ,function2 "And" expr expr andFun
  ,function2 "Or" expr expr orFun
  ,function2 "Iff" expr expr (\a b -> 
        andFun (orFun (notFun a) b) (orFun a (notFun b)))
  ,liftM notFun (function1 "Not" expr)]

{- | 'expr' matches a var or an apply -}
expr :: Parser PreExpression
expr = liftM V (try var) <|> try apply <|> (function0 "Undefined" >> return Undefined)

{- | 'assign' matches \"Assign(\<'var'\>,\<'expr'\>)\". -}
assign :: Parser PreAssign
assign = function2 "Assign" var expr Assign

{- | 'assert' matches \"Assert(\<'expr'\>)\". -}
assert :: Parser PreAST
assert = function2 "Assert" expr (brcks (function1 "Src" quotedString))  (\a _ ->Assert a)

{- | 'block' matches \"Block(\<'var'\>,\<'listOf' 'expr'\>)\". -}
block :: Parser PreAST
block = liftM Block $ function1 "Block" (listOf ast)

{- | 'ifstatement' matches \"If(\<'expr'\>,\<'ast'\>,\<'ast'\>)\". -}
ifstatement :: Parser PreAST
ifstatement = function3 "If" expr mayast mayast If

mayast :: Parser PreAST
mayast = do
  symbol "["
  a <- ast <|> return (Block [])
  symbol "]"
  return a

{- | 'ast' matches either an 'ifstatement', a 'block' or an 'assign'. -}
ast :: Parser PreAST
ast =
  try ifstatement 
  <|> try block
  <|> try (function0 "Nop" >> return (Block []))
  <|> try letBlock
  <|> try assert
  <|> liftM Assig assign

{- | 'letBlock' matches \"Let(\<'varDecl'\>,\<'listOf' 'ast'\>)\". -}
letBlock :: Parser PreAST
letBlock = function2 "Let" varDecl (listOf ast) Let

{- | 'varDecl' matches \"[VarDecl(\<'mode'\>,\<'name'\>,<'typeIdent'>,<'ast'>)]\". -}
varDecl :: Parser (VarDecl ASTVarAnon ASTFunAnon)
varDecl = do
  function4 "VarDecl" mode name typeIdent expr (\m n t a -> VarDecl m (ASTVarAnon False n t) a)

{- | 'axiom' matches \"Axiom(\<'listOf' 'quantVar'\>,\<'expr'\>)\". -}
axiom :: Parser PreAxiom
axiom = function3 "Axiom" quotedString (listOf var) expr (const Forall)


{- | 'body' matches \"Body(<'ast'>)\".-}
body :: Parser PreAST
body = function1 "Body" ast

{- | 'body' matches 
 - \"ProcClause(ProcName(<'name'>,<'listOf' 'anonParam'>),
 - ParamList(<'listOf' 'param'>))\".-}
procClause :: Parser (PreAST -> PreDefinition)
procClause = 
  function2 "ProcClause"
    (function2 "ProcName"
      name
      (listOf anonParam)
      ProcDecl)
    (function1 "ParamList" (listOf param))
    (\d p a -> ProcD (ProcDef d p a))


{- | 'definition' matches either
 -    \"Define(<'funClause'>,Body(<'expr'>))\"
 -    or
 -    \"Define(<'procClause'>,Body(<'ast'>))\". -}
definition :: Parser PreDefinition
definition = do
  symbol "Define"
  symbol "("
  f1 <-  liftM Left funClause <|> liftM Right procClause
  symbol ","
  f2 <- either (\f -> liftM f (function1 "Body" expr))
               (\f -> liftM f (function1 "Body" ast)) f1
  symbol ")"
  return f2

{- | 'funClause' matches
 -   \"FunClause(<'funName'>,ParamList(<'listOf' 'param'>))\" -} 
funClause :: Parser (PreExpression -> PreDefinition)
funClause = 
  function3 "FunClause" Parser.funName 
  (function1 "ParamList" (listOf param)) typeIdent 
  (\a b c e -> FunD (FunDef (FunClause a{funType = c} b c) e))

{- | inputParser matches expressions on the
 - form ToTransform(\<'listOf' 'definition'\>, \<'listOf' 'axiom'\>). -}
inputParser :: Parser ([PreDefinition], [PreAxiom])
inputParser =
  function2 "ToTransform" (listOf definition) (listOf axiom) (,)
