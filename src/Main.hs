{-# LANGUAGE NoMonomorphismRestriction, DeriveDataTypeable #-}
module Main where
import Text.Parsec.String(parseFromFile)
import System.Console.CmdArgs
import Control.Monad
import qualified Data.Map as M
import Data.Map(Map)
import Data.Maybe

import DOT(printDot)
import Parser(inputParser)
import Z3
import Z3.Output
import Z3.RewriteRules
import Structure.CFG
import Structure.AST
import Structure.SSA
import Algorithm.LiveVariableAnalysis


-- | The Record containing
-- all input parameters.
data Rewriter = Rewriter {
  z3Location :: String,
  inFile :: String,
  useAxiomSuggestions :: Bool,
  dotFile :: Maybe String} deriving (Show,Data,Typeable)

-- | The definition of the input parameters to be parsed by
-- cmdArgs
rewriter :: Rewriter
rewriter = Rewriter{
  z3Location = 
    def &=
    argPos 0 &=
    typ "Z3LOCATION",
  inFile = 
    def &= 
    argPos 1 &=
    typ "INPUTFILE",
  useAxiomSuggestions = 
    def &= 
    explicit &= 
    help "Turns on using axioms as suggestions for rewriting" &=
    name "a" &= 
    name "axiom-suggestions",
  dotFile = 
   def &= 
   help "Generate a graphviz dotfile with the given filename" &=
   typFile}
  &= summary
    "From the description of the program given in INPUTFILE\
    \ the program uses z3 (the location of the executable should\
    \ be given as Z3LOCATION) to find equivalent assignments in the program."


main :: IO ()
main = do
  (Rewriter z3Loc infile useAxSuggest mayOutfile) <- cmdArgs rewriter
  parsedSol <- parseFromFile inputParser infile
  case parsedSol of
    (Left err)   -> print err 
    (Right (preDefs,preAxs)) -> do
      let (defs,axioms,prefunMap,varMap,n) = preASTtoAST (preDefs,preAxs)
      let funMap = specialize prefunMap
      let (n2,cfgs) = defsToCfgs funMap varMap n defs
      let (_,ssas) = toSSAs n2 cfgs
      let allvars = M.unions (map cfgVarAnons ssas)
      let allfuns = M.unions (map cfgFunAnons ssas)
      let funDefs = filterFunDef defs
      let z3Command = z3Loc ++ " -smt2 -t:100 -in"
      z3Out <-
        forM ssas (\ssa -> do
          let phiaxioms = getPhiAxioms ssa
          let allAxioms = phiaxioms ++ axioms
          if useAxSuggest
            then do
              let axSuggs = concatMap axiomToSuggestion allAxioms
              getEquivalentDefs funDefs z3Command ssa allAxioms axSuggs
            else 
              getEquivalentDefs funDefs z3Command ssa allAxioms []
        )
      putStrLn (showOutputs allvars allfuns z3Out)
--      print ssas
--      printLiveData ssas
      case mayOutfile of
        Nothing -> return ()
        Just outfile -> 
          forM_ z3Out (writeFile outfile . printDot)
--  putStrLn =<< readFile infile

printLiveData :: [SSA] -> IO ()
printLiveData =
  print . map livenessAnalysis

-- | Turns the given cfgs into SSAs
toSSAs :: Integer -> [CFG] -> (Integer, [SSA])
toSSAs n2 = foldl go (n2,[])
  where 
  go (n,ssas) cfg = 
    let ssa = unrollCompoundExprs (cfgToSSA cfg {cfgNextId = n})
    in (cfgNextId ssa,ssa:ssas)

filterFunDef :: [Definition Var Fun] -> [FunDef Var Fun]
filterFunDef = mapMaybe go
  where
   go ProcD{} = Nothing
   go (FunD fd) = Just fd

-- | Turns a list of definitions into a CFG
-- note that only Procedures are turned into
-- CFGs
defsToCfgs ::
  Map Fun ASTFunAnon
  -> Map Var ASTVarAnon
  -> Integer
  -> [Definition Var Fun]
  -> (Integer, [CFG])
defsToCfgs fm vm n = foldl go (n,[])
  where
   go (n1,xs) FunD{} = (n1,xs)
   go (n1,xs) (ProcD pd) =
     let cfg = astToCFG pd fm vm n1
     in (cfgNextId cfg,cfg:xs)
 
