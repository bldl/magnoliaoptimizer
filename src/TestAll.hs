module Main where
{- | This test module runs all
 - the tests in the project.
 - Can be built by
 - > cabal configure --enable-tests
 - > cabal build
 - and then run by 
 - > cabal test -}

import System.Exit (exitFailure)
import Test.QuickCheck
import Control.Monad

import Structure.Tests.CFG
import Structure.Tests.AST
import Structure.Tests.SSA
import Algorithm.Tests.Idom
import Algorithm.Tests.DominatingFrontier


isFailure :: Result -> Bool
isFailure (Failure {}) = True
isFailure (GaveUp {}) = True
isFailure (NoExpectedFailure {}) = True
isFailure _            = False

main :: IO ()
main = do
  bs <- sequence 
    [quickCheckResult testPreASTToAST,
     quickCheckResult testEvalEqualASTCfg,
     quickCheckResult onlyOneAssignment,
     quickCheckResult testEvalEqualASTSsa,
     quickCheckResult testAllElemsContained,
     quickCheckResult existsPathFromIdom,
     quickCheckResult existsPathFromSdom,
     quickCheckResult existsPathFromIdomToSdom,
     quickCheckResult idomCoOnPaths,
     quickCheckResult thm3Lengauer]
  when (any isFailure bs) exitFailure
