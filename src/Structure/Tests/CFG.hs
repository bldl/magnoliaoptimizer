{-# LANGUAGE FlexibleInstances #-}
module Structure.Tests.CFG where

import Test.QuickCheck
import Control.Monad
import Control.Monad.State

import Structure.CFG
import Structure.AST
import Structure.Tests.AST
import Structure.DAG

data ArithVar = A | B | C | D | E deriving (Eq,Ord,Show)
data ArithFun = 
  Pluss 
  | Minus
  | Multiply
  | ArithOr
  | ArithAnd
  | ArithNot
  | ArithFalse
  | ArithTrue deriving (Eq,Ord,Show)

instance HasBoolSym ArithFun where
  orSym = ArithOr
  andSym = ArithAnd
  notSym = ArithNot
  falseSym = ArithFalse
  trueSym = ArithTrue

instance Arbitrary ArithVar where
  shrink = shrinkNothing
  arbitrary = elements [A,B,C,D,E]

instance Arbitrary ArithFun where
  shrink = shrinkNothing
  arbitrary = elements [Pluss,Minus,Multiply]

instance Arity ArithFun where
   arity ArithNot = 1
   arity ArithFalse = 0
   arity ArithTrue = 0
   arity _         = 2

data VarValues = VV {
  aValue :: Integer,
  bValue :: Integer,
  cValue :: Integer,
  dValue :: Integer,
  eValue :: Integer} deriving (Eq,Ord,Show)

instance Arbitrary VarValues where
  shrink = shrinkNothing
  arbitrary = liftM5 VV arbitrary arbitrary arbitrary arbitrary arbitrary

type ArithCFG = Cfg ArithVar ArithFun ()
type ArithAST = Ast ArithVar ArithFun

emptyProc = ProcClause (ProcAnon "" []) []

instance (Ord a, Ord b,Arity b,Arbitrary a, Arbitrary b,HasBoolSym b) =>
   Arbitrary (Cfg a b ()) where
  shrink = shrinkNothing
  arbitrary = do
    preAST <- arbitrary
    let ([ProcClause _ _ ast],_,funmap,vmap,n) = preASTtoAST ([emptyProc preAST],[])
    return (astToCFG ast funmap vmap n)

testEvalEqualASTCfg :: ArithAST -> VarValues -> Bool
testEvalEqualASTCfg ast vv = 
  let astResult = execState (evalArithAst ast) vv in
  let ([ProcClause _ _ ptrAst],_,fm,vm,n) = preASTtoAST ([emptyProc ast], []) in
  let cfg = astToCFG ptrAst fm vm n in
  let cfgResult = execState (evalArithCfg cfg (root cfg)) vv in
    cfgResult == astResult

lookupDefAssign :: ArithCFG -> Assignment -> Assign ArithVar ArithFun
lookupDefAssign cfg (Assign v expr) =
  Assign  (varAnnot v cfg) (lookupDefExpr cfg expr)

lookupDefExpr :: ArithCFG -> Expression -> Expr ArithVar ArithFun
lookupDefExpr cfg (Ite a b c) =
  let a' = lookupDefExpr cfg a
      b' = lookupDefExpr cfg b
      c' = lookupDefExpr cfg c in
       Ite a' b' c'
lookupDefExpr cfg (Eq a b) =
  let a' = lookupDefExpr cfg a
      b' = lookupDefExpr cfg b in
      Eq a' b'
lookupDefExpr cfg (V v) = V (varAnnot v cfg)
lookupDefExpr cfg (Apply f exprs) = Apply (funAnnot f cfg) (map (lookupDefExpr cfg) exprs)

evalArithCfg :: ArithCFG -> CFGPtr -> State VarValues ()
evalArithCfg _ 0 = return ()
evalArithCfg cfg r = do
  mapM_ evalArithAssig . map (lookupDefAssign cfg) . assignments r $ cfg
  let c1 = trueChild r cfg
  let c2 = falseChild r cfg
  b <- evalArithExpr (lookupDefExpr cfg . condition r $ cfg)
  evalArithCfg cfg (if b == 0 then c2 else c1)
    
evalArithAst :: ArithAST -> State VarValues ()
evalArithAst (If e a b) = do 
  v <- evalArithExpr e
  evalArithAst (if v == 0 then b else a)
evalArithAst (Block xs) = mapM_ evalArithAst xs
evalArithAst (Assig a) = evalArithAssig a

evalArithAssig :: Assign ArithVar ArithFun -> State VarValues ()
evalArithAssig (Assign v expr) = do
  value <- evalArithExpr expr
  case v of
    A -> modify (\s -> s{aValue = value}) 
    B -> modify (\s -> s{bValue = value})
    C -> modify (\s -> s{cValue = value})
    D -> modify (\s -> s{dValue = value})
    E -> modify (\s -> s{eValue = value})

evalArithExpr :: Expr ArithVar ArithFun -> State VarValues Integer
evalArithExpr (V A) = gets aValue
evalArithExpr (V B) = gets bValue
evalArithExpr (V C) = gets cValue
evalArithExpr (V D) = gets dValue
evalArithExpr (V E) = gets eValue
evalArithExpr (Ite a b c) = do
  g <- evalArithExpr a
  evalArithExpr (if g == 0 then c else b)
evalArithExpr (Eq a b) = do
  a' <- evalArithExpr a
  b' <- evalArithExpr b
  return (if a' == b' then 1 else 0)
evalArithExpr (Apply Pluss [a,b])    =
  liftM2 (+) (evalArithExpr a) (evalArithExpr b)
evalArithExpr (Apply Minus [a,b])    =
  liftM2 (-) (evalArithExpr a) (evalArithExpr b)
evalArithExpr (Apply Multiply [a,b]) =
 liftM2 (*) (evalArithExpr a) (evalArithExpr b)
evalArithExpr (Apply ArithFalse []) = return 0
evalArithExpr (Apply ArithTrue []) = return 1
evalArithExpr (Apply ArithOr [a,b]) =
 liftM2 aOr (evalArithExpr a) (evalArithExpr b)
evalArithExpr (Apply ArithAnd [a,b]) =
 liftM2 aAnd (evalArithExpr a) (evalArithExpr b)
evalArithExpr (Apply ArithNot [a]) =
 liftM aNot (evalArithExpr a)
evalArithExpr (Apply _ _) =
  error "Failed Test: No matching Arith function in eval!"

aNot :: Integer -> Integer 
aNot 0 = 1
aNot _ = 0
aOr :: Integer -> Integer -> Integer
aOr a 0 = a
aOr 0 b = b
aOr a _ = a
aAnd :: Integer -> Integer -> Integer
aAnd 0 _ = 0
aAnd _ 0 = 0
aAnd a _ = a
