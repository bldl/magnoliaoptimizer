module Structure.Tests.SSA where

import qualified Data.Map as M
import qualified Data.Set as S
import           Control.Monad.State
import           Data.Maybe

import Structure.SSA
import Structure.CFG
import Structure.AST
import Structure.DAG
import Structure.Tests.CFG


type ArithSSA = Cfg (SSAVA ArithVar) (SSAFA ArithFun) (M.Map CFGPtr [PhiAssign])

data EvalArithSSAState = EASSAS {
  origVarValues :: VarValues,
  varValues :: M.Map Var Integer,
  latestValues :: VarValues
  } deriving (Eq,Ord,Show)

evalArithSSA :: ArithSSA -> State EvalArithSSAState ()
evalArithSSA ssa = go (cfgRoot ssa)
  where
    go 0 = return ()
    go r = do
      mapM_ (evalPhi ssa) (phiFunctions r ssa)
      mapM_ (evalAssig ssa) (assignments r ssa)
      b <- evalExpr ssa (condition r ssa)
      if b == 0
        then go (falseChild r ssa)
        else go (trueChild r ssa)

findVar :: ArithSSA -> Var -> SSAVA ArithVar
findVar ssa v = cfgVarAnons ssa M.! v

evalExpr :: ArithSSA -> Expression -> State EvalArithSSAState Integer
evalExpr ssa (V v) = getValue (findVar ssa v)
evalExpr ssa (Eq a b) = do
  av <- evalExpr ssa a 
  bv <- evalExpr ssa b 
  return (if av /= bv then 0 else 1)
evalExpr ssa (Ite a b c) = do
  g <- evalExpr ssa a
  evalExpr ssa (if g == 0 then c else b)
evalExpr ssa (Apply f []) = do
  o <- getOrigFunction ssa f
  case o of
    ArithFalse -> return 0
    ArithTrue -> return 1
    _         -> error "Failed Test: No matching 0-arith function"
evalExpr ssa (Apply f [a]) = do
  o <- getOrigFunction ssa f
  case o of
    ArithNot ->   liftM aNot (evalExpr ssa a)
    _        -> error "Failed Test: No matching 1-arith function"
evalExpr ssa (Apply f [a,b]) = do
  o <- getOrigFunction ssa f
  case o of
    ArithAnd ->   liftM2 aAnd (evalExpr ssa a) (evalExpr ssa b)
    ArithOr ->   liftM2 aOr (evalExpr ssa a) (evalExpr ssa b)
    Pluss ->     liftM2 (+) (evalExpr ssa a) (evalExpr ssa b)
    Minus ->     liftM2 (-) (evalExpr ssa a) (evalExpr ssa b)
    Multiply ->  liftM2 (*) (evalExpr ssa a) (evalExpr ssa b)
    _        -> error "Failed Test: No matching 2-arith function"

evalExpr _ (Apply _ _) =
  error "Failed Test: eval due to too many params to arith function"

getValue :: SSAVA ArithVar -> State EvalArithSSAState Integer
getValue  (SSAVarAnon A _) = gets (aValue . latestValues)
getValue  (SSAVarAnon B _) = gets (bValue . latestValues)
getValue  (SSAVarAnon C _) = gets (cValue . latestValues)
getValue  (SSAVarAnon D _) = gets (dValue . latestValues)
getValue  (SSAVarAnon E _) = gets (eValue . latestValues)

findAssigned :: ArithSSA -> [Var] -> State EvalArithSSAState Var
findAssigned _   [] = error "No Assigned var in a phi func."
findAssigned ssa (p:ps) = do
  vv <- gets varValues
  let (SSAVarAnon _ m) = cfgVarAnons ssa M.! p
  if M.member p vv || isNothing m
    then return p
    else findAssigned ssa ps

evalPhi :: ArithSSA -> PhiAssign -> State EvalArithSSAState ()
evalPhi ssa (PhiAssign v _ ps) = do
  p <- findAssigned ssa ps
  val <- getValue (findVar ssa p)
  modify (\s -> s{varValues = M.insert v val (varValues s)})

evalAssig :: ArithSSA -> Assignment -> State EvalArithSSAState ()
evalAssig ssa (Assign v expr) = do
  val <- evalExpr ssa expr
  let ov = origVar (cfgVarAnons ssa M.! v)
  modify (\s -> s{
    varValues = M.insert v val (varValues s),
    latestValues = changeValue ov val (latestValues s)
    })

changeValue :: ArithVar -> Integer -> VarValues -> VarValues
changeValue A new vv = vv{aValue = new}
changeValue B new vv = vv{bValue = new}
changeValue C new vv = vv{cValue = new}
changeValue D new vv = vv{dValue = new}
changeValue E new vv = vv{eValue = new}

getOrigFunction :: ArithSSA -> Fun -> State EvalArithSSAState ArithFun
getOrigFunction ssa f =
  case (cfgFunAnons ssa M.! f) of
    (SSAFun a) -> return a
    (PhiFun _) -> error "Not an orig var."

testEvalEqualASTSsa :: ArithAST -> VarValues -> Bool
testEvalEqualASTSsa ast vv =
  let astResult = execState (evalArithAst ast) vv in
  let ([ProcClause _ _ ptrAst],_,fm,vm,n) = preASTtoAST ([emptyProc ast], []) in
  let cfg = astToCFG ptrAst fm vm n in
  let ssa = cfgToSSA cfg in
  let ssaResults = latestValues $ execState (evalArithSSA ssa) (EASSAS vv M.empty vv) in
    (astResult == ssaResults)


onlyOneAssignment :: ArithAST -> Bool
onlyOneAssignment ast = evalState (go (root ssa)) (S.empty,S.empty)
  where 
    go r = do
      alreadyVisited <- liftM (r `S.member`) . gets $ snd
      if alreadyVisited 
        then return True
        else do
          let phis = fromMaybe [] . M.lookup r . cfgPhiFunctions $ ssa
          phisOk <- liftM and . mapM (\(PhiAssign v _ _) -> do
            b <- liftM (v `S.notMember`) (gets fst)
            modify (\(vars,funs) -> (S.insert v vars, funs))
            return b) $ phis
          assigsOk <- liftM and . mapM (\(Assign v _) -> do
            b <- liftM (v `S.notMember`) (gets fst)
            modify (\(vars,funs) -> (S.insert v vars, funs))
            return b
            ) . assignments r $ ssa
          modify (\(vars,funs) -> (vars,S.insert r funs))
          childrenOk <- liftM and . mapM go . children r $ ssa
          return (phisOk && assigsOk && childrenOk)
    ssa = cfgToSSA (astToCFG ptrAst fm vm n)
    ([ProcClause _ _ ptrAst],_,fm,vm,n) = preASTtoAST ([emptyProc ast], [])

