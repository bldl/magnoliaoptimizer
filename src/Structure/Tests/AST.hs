module Structure.Tests.AST where

import Structure.AST

import           Test.QuickCheck
import           Control.Monad
import qualified Data.Map as M

class Arity a where
  arity :: a -> Int

instance Arity ASTFunAnon where
  arity = length . params

instance (Arbitrary a, Arbitrary b,Arity b) => Arbitrary (Forall a b) where
   shrink = shrinkNothing
   arbitrary = liftM2 Forall arbitrary arbitrary

instance (Arbitrary a, Arbitrary b,Arity b) => Arbitrary (Definition a b) where
  shrink = shrinkNothing
  arbitrary = frequency 
    [(3,liftM3 ProcClause arbitrary arbitrary arbitrary ),
    (1,liftM3 FunClause arbitrary arbitrary arbitrary)]

instance Arbitrary ProcAnon where
  shrink = shrinkNothing
  arbitrary = liftM2 ProcAnon arbitrary arbitrary

instance (Arbitrary a) => Arbitrary (Param a) where
  shrink = shrinkNothing
  arbitrary = liftM2 Param arbitrary arbitrary

instance (Arbitrary a, Arbitrary b,Arity b) => Arbitrary (Ast a b) where
  shrink (If _ a b) = [a,b]
  shrink (Block xs) = xs
  shrink _ = []
  arbitrary =
   frequency 
     [(30,liftM Assig arbitrary),
      (2,liftM3 If arbitrary arbitrary arbitrary),
      (1,choose (0,3) >>= (\n -> liftM Block (replicateM n arbitrary)))]
         

instance (Arbitrary a, Arbitrary b,Arity b) => Arbitrary (Expr a b) where
  shrink (Apply _ e) = e
  shrink _ = []
  arbitrary =
   frequency [(30,liftM V arbitrary),
              (1,do 
                 f <- arbitrary
                 let n = arity f
                 liftM (Apply f) (vector n))]

instance (Arbitrary a, Arbitrary b,Arity b) => Arbitrary (Assign a b) where
  shrink = shrinkNothing
  arbitrary = liftM2 Assign arbitrary arbitrary

instance Arbitrary ASTVarAnon where
  shrink = shrinkNothing
  arbitrary = liftM2 (ASTVarAnon False) arbitrary arbitrary
     
instance Arbitrary ASTFunAnon where
  shrink = shrinkNothing
  arbitrary =
    liftM4 ASTFunAnon arbitrary arbitrary arbitrary arbitrary

instance Arbitrary Type where
  shrink = shrinkNothing
  arbitrary = liftM Type arbitrary

instance Arbitrary AnonParam where
  shrink = shrinkNothing
  arbitrary = liftM2 AnonParam arbitrary arbitrary

instance Arbitrary Mode where
  shrink = shrinkNothing
  arbitrary = do
    i <- choose (0,2) :: Gen Integer
    case i of
      0 -> return Obs
      1 -> return Out
      2 -> return In
      _ -> error "Unexpected exception when generating mode."

testPreASTToAST :: PreDefinition -> Bool
testPreASTToAST preDef@(FunClause{}) = null ax
  where
   (def ,ax,_,_,_) = preASTtoAST ([preDef],[])
testPreASTToAST preDef@(ProcClause _ _ preAst) = 
   case def of
     [ProcClause _ _ ast] -> null ax && go ast preAst
     _                    -> False
  where 
   (def ,ax,fm,vm,_) = preASTtoAST ([preDef],[])
   go (If e a b) (If e' a' b') = goExpr e e' && and (zipWith go [a,b] [a',b'])
   go (Block xs) (Block xs') = and $ zipWith go xs xs'
   go (Assig (Assign v expr)) (Assig (Assign v' expr')) = vm M.! v == v' && goExpr expr expr'
   go _ _ = False -- Structure missmatch
   goExpr (V a) (V a') = vm M.! a == a'
   goExpr (Apply f expr) (Apply f' expr') = fm M.! f == f' && and (zipWith goExpr expr expr')
   goExpr _ _ = False -- Structure missmatch
