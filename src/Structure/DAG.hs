module Structure.DAG where

type Vertex = Integer

class DAG a where
  children :: Vertex -> a -> [Vertex]
  numNodes :: a -> Integer
  root     :: a -> Vertex

