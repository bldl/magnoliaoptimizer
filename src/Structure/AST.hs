{-# LANGUAGE FlexibleInstances #-}
{- | This module defines the Abstract Syntax Tree (AST), 
     for the input language. Every part of the AST is parametrized
     by what is beeing used to represent variables and functions.-}
module Structure.AST
  (Ast (..),
   PreAST,
   AST,
   Forall (..),
   PreAxiom,
   Axiom,
   Assign (..),
   Assignment,
   PreAssign,
   Expr (..),
   Expression,
   Statement(..),
   PreExpression,
   PreDefinition,
   Definition(..),
   FunDef(..),
   FunClause(..),
   ProcDef(..),
   ProcDecl(..),
   Fun (..),
   andFun,
   orFun,
   notFun,
   trueFun,
   falseFun,
   ASTFunAnon (..),
   Var (..),
   ASTVarAnon (..),
   Type (..),
   AnonParam (..),
   Param(..),
   Mode (..),
   VarDecl(..),
   Name,
   getVars,
   specialize,
   preASTtoAST,
   HasBoolSym(..),
   substitute)
  where

import           Control.Monad
import           Control.Monad.State
import qualified Data.Map as M
import           Data.Maybe
import qualified Data.Set as S

type Name = String

type PreDefinition = Definition ASTVarAnon ASTFunAnon

data Definition v f = 
  FunD (FunDef v f)
 | ProcD (ProcDef v f) deriving (Eq,Ord,Show)

data FunDef v f = FunDef (FunClause v f) (Expr v f) deriving (Eq,Ord,Show)
data ProcDef v f = ProcDef{
  procDecl :: ProcDecl,
  procParams ::  [Param v],
  procAST ::  Ast v f} deriving (Eq,Ord,Show)
data ProcDecl = ProcDecl String [AnonParam]  deriving (Eq,Ord)
data FunClause v f = FunClause f [Param v] Type deriving (Eq,Ord,Show)

instance Show ProcDecl where
  show (ProcDecl name ps) =
   "Proc(ProcName(Name(" ++ show name ++ "))," ++ 
     show ps ++ ")"


data VarDecl v f = VarDecl Mode v (Expr v f) deriving(Eq,Ord,Show)

{- | An 'Ast' defines the abstract syntax tree. The Ast type is 
     parametrized by what represents variables and functions.-}
data Ast v f =
  If (Expr v f) (Ast v f) (Ast v f) 
  | Block [Ast v f] 
  | Assig (Assign v f) 
  | Let (VarDecl v f) [Ast v f]
  | Assert (Expr v f)
  | Assume (Expr v f) deriving (Eq,Ord,Show)

type PreAST = Ast ASTVarAnon ASTFunAnon
type AST = Ast Var Fun

{- | An 'Forall' defines an axiom.-}
data Forall v f = Forall [v] (Expr v f) deriving (Eq,Ord,Show)
type PreAxiom = Forall ASTVarAnon ASTFunAnon
type Axiom = Forall Var Fun

{- | An 'Assign' defines an assignment of a variable to the value of some
 -   expression. -}
data Assign v f = Assign{
  assignVar :: v, -- ^ The variable being assigned.
  assignExpr :: Expr v f -- ^ The expression the 'assignVar' is being assigned to.
  }  deriving (Eq,Ord)

instance (Show v,Show f) => Show (Assign v f) where
  show (Assign v e) = "Assign(" ++ show v ++ "," ++ show e ++ ")"

type PreAssign = Assign ASTVarAnon ASTFunAnon
type Assignment = Assign Var Fun
data Statement =
  StAssign Assignment |
  StAssert Expression |
  StAssume Expression deriving (Eq,Ord,Show)

{- | 'Expr' defines an expression.-} 
data Expr v f = 
  V v  -- ^ A Variable.
  | Apply f [Expr v f] -- ^ A function f applied to a list of parameters.
  | Eq (Expr v f) (Expr v f) -- ^ The Equality Predicate.
  | Ite (Expr v f) (Expr v f) (Expr v f) -- ^ If-then-else
  | Undefined  -- ^ An undefined value. The value of an unassigned variable.
    deriving (Eq,Ord)

instance (Show v, Show f) => Show (Expr v f) where
  show Undefined = "Undefined"
  show (V v) = "Var(" ++ show v ++ ")"
  show (Apply f es) = "Apply(" ++ show f ++ "," ++ show es ++ ")"
  show (Eq a b) = "(" ++ show a ++ " = " ++ show b ++ ")"
  show (Ite a b c) = unwords ["If",show a,"then",show b,"else",show c,"end"]

getVars :: (Ord v) => Expr v f -> S.Set v
getVars (V v) = S.singleton v
getVars (Apply _ es) = S.unions . map getVars $ es
getVars (Eq a b) = S.unions . map getVars $ [a,b]
getVars (Ite a b c) = S.unions . map getVars $ [a,b,c]
getVars Undefined = S.empty

type PreExpression = Expr ASTVarAnon ASTFunAnon
type Expression = Expr Var Fun

{- | 'Fun' is an index to a function. -}
data Fun = 
  Fun Integer -- | A uninterpreted function with id 
  | Or          -- | The boolean Function Or
  | And         -- | The boolean Function And
  | Not         -- | The boolean Function Not
  | ConstFalse  -- | The Constant False function
  | ConstTrue  -- | The Constant False function
    deriving (Eq,Ord)
instance Show Fun where
  show (Fun n) =  "f_" ++ show n
  show And =  "and"
  show Or =  "or"
  show Not =  "not"
  show ConstFalse =  "false"
  show ConstTrue =  "true"

class HasBoolSym sym where
  andSym :: sym
  orSym :: sym
  notSym :: sym
  falseSym :: sym
  trueSym :: sym

instance HasBoolSym Fun where
  andSym = And
  orSym = Or
  notSym = Not
  falseSym = ConstFalse
  trueSym = ConstTrue

instance HasBoolSym ASTFunAnon where
  andSym = 
    ASTFunAnon
      "_&&_" 
      [AnonParam Obs Predicate,AnonParam Obs Predicate]
      Predicate
      (Just "and")
  orSym = 
    ASTFunAnon
      "_||_" 
      [AnonParam Obs Predicate,AnonParam Obs Predicate]
      Predicate
      (Just "or")
  notSym = 
    ASTFunAnon
      "!_" 
      [AnonParam Obs Predicate]
      Predicate
      (Just "not")
  trueSym =
    ASTFunAnon
      "true" 
      []
      Predicate
      (Just "true")
  falseSym = 
    ASTFunAnon
      "false" 
      []
      Predicate
      (Just "false")


preDefFuns :: (HasBoolSym f) => M.Map Fun f
preDefFuns = M.fromList
  [(Or,orSym),
   (And,andSym),
   (Not,notSym),
   (ConstFalse,falseSym),
   (ConstTrue,trueSym)]


andFun :: (HasBoolSym f) => Expr v f -> Expr v f -> Expr v f
andFun a b = Apply andSym [a,b]
orFun :: (HasBoolSym f) => Expr v f -> Expr v f -> Expr v f
orFun a b = Apply orSym [a,b]
notFun :: (HasBoolSym f) => Expr v f -> Expr v f
notFun a = Apply notSym [a]
trueFun :: (HasBoolSym f) => Expr v f
trueFun = Apply trueSym []
falseFun :: (HasBoolSym f) => Expr v f
falseFun = Apply falseSym []

{- | 'ASTFunAnon' is the annotations given
     to a function from input. -}
data ASTFunAnon = ASTFunAnon{
  funName :: Name -- ^ The name of the function.
  ,params  :: [AnonParam] -- ^ A list of parameter annotations.
  ,funType :: Type -- ^ The return type of the function.
  ,smtName :: Maybe String -- ^ Just a if the function corresponds
                           -- to the smt function a. Nothing otherwise.
  } deriving (Eq,Ord)

instance Show ASTFunAnon where
  show (ASTFunAnon n p t _) = "Fun(FunName(Name(" ++ n ++ ")," ++ show p ++ "," ++ show t ++ ")"

{- | 'Var' is an index to a variable. -}
data Var = Var{
  index :: Integer} deriving (Eq,Ord)

{- | 'ASTVarAnon' is the annotation given
      to a variable from input. -}
data ASTVarAnon = ASTVarAnon {
 quantified :: Bool, -- ^ Whether or not the variable is scoped by a quantifier.
 varName :: Name, -- ^ The name of the variable.
 varType :: Type -- ^ The type of the variable.
 } deriving (Eq,Ord)

instance Show ASTVarAnon where
  show (ASTVarAnon _ n t) = "Var(VarName(Name(" ++ n ++ ")," ++ show t ++ "))"

instance Show Var where
  show (Var n) = "x_" ++ show n

{- | 'Type' represents a type in the input format -}
data Type = Type String | Predicate  deriving (Eq,Ord)

instance Show Type where
  show Predicate = "Predicate()"
  show (Type n) = "Type(Name(" ++ n ++ "))" 


{- | 'Param' is the annotations given to each
     parameter from input. -}
data Param v = Param{
  pmode :: Mode,
  pvar :: v,
  ptype  :: Type} deriving (Eq,Ord,Show)

{- | 'AnonParam' is the parameters associated with a function
 - or procedure declaration. Therefore it does not contain
 - any reference to a variable. -}
data AnonParam = AnonParam Mode Type deriving (Eq,Ord)


instance Show AnonParam where
  show (AnonParam m t) = "AnonParam(" ++ show m ++ "," ++ show t ++ ")"

{- | 'Mode' represents the mode of a parameter,
    'Obs' for observable,
    'Out' for output variable,
    'In' for input variable. -}
data Mode = Obs | Out | In | Upd deriving (Eq,Ord,Show)


substitute :: (Eq v) => v -> v -> Expr v f -> Expr v f
substitute v1 v2 (V v3) = V (if v1 == v3 then v2 else v3)
substitute v1 v2 (Eq a b) =
  Eq (substitute v1 v2 a) (substitute v1 v2 b)
substitute v1 v2 (Ite a b c) =
  Ite (substitute v1 v2 a) (substitute v1 v2 b) (substitute v1 v2 c)
substitute v1 v2 (Apply f es) =
  Apply f (map (substitute v1 v2) es)
substitute _ _ Undefined = Undefined

specialize :: M.Map Fun ASTFunAnon -> M.Map Fun ASTFunAnon
specialize = M.map specializeFun


specializeFun :: ASTFunAnon -> ASTFunAnon
specializeFun f@ASTFunAnon{funName = "one"}  = f{smtName = Just "1"}
specializeFun f@ASTFunAnon{funName = "zero"} = f{smtName = Just "0"}
specializeFun f@ASTFunAnon{funName = "!_"}   = f{smtName = Just "not"}
specializeFun f@ASTFunAnon{funName = "_>_"}  = f{smtName = Just ">"}
specializeFun f@ASTFunAnon{funName = "_<_" } = f{smtName = Just "<"}
specializeFun f@ASTFunAnon{funName = "_<=_"} = f{smtName = Just "<="}
specializeFun f@ASTFunAnon{funName = "_>=_"} = f{smtName = Just ">="}
specializeFun f@ASTFunAnon{funName = "-_"}  = f{smtName = Just "-"}
specializeFun f@ASTFunAnon{funName = "_-_"}  = f{smtName = Just "-"}
specializeFun f@ASTFunAnon{funName = "_*_"}  = f{smtName = Just "*"}
specializeFun f@ASTFunAnon{funName = "_+_"}  = f{smtName = Just "+"}
specializeFun f@ASTFunAnon{funName = "_/_"}  = f{smtName = Just "div"}
specializeFun f@ASTFunAnon{funName = "_==_"} =  f{smtName = Just "="}
specializeFun f@ASTFunAnon{funName = "_===_"} =  f{smtName = Just "="}
specializeFun f@ASTFunAnon{funName = "_&&_"} = f{smtName = Just "and"}
specializeFun f@ASTFunAnon{funName = "_||_"} = f{smtName = Just "or"}
specializeFun f@ASTFunAnon{funName =  "ite"} = f{smtName = Just "ite"}
specializeFun f = f


type ScopeId = Integer

data ASTTS a b = ASTTS {
  insertedFuns :: M.Map b Fun,
  insertedVars :: M.Map (a,ScopeId) Var,
  funMap :: M.Map Fun b,
  varMap :: M.Map Var a,
  currentScopes :: [ScopeId],
  nextScope :: ScopeId,
  nextId :: Integer} deriving (Eq,Ord,Show)

{- | 'preASTtoASTGeneric' traverses the given axioms and ast and
  replaces occurences of variables and functions with the index types Var and
  Fun. Returns the altert ast and axioms together with maps from Vars and
  Funs to the original variable and function representations, and the next
  \"fresh\" index. -}
preASTtoAST:: (Ord v,Ord f,HasBoolSym f) => 
  ([Definition v f],[Forall v f])
  -> ([Definition Var Fun],[Axiom],M.Map Fun f,M.Map Var v,Integer)
preASTtoAST (pAst,pAx) = 
  (ast,ax,funMap result,varMap result,nextId result) 
  where
    ((ast,ax),result) = runState go emptyASTTS
    defFuns = preDefFuns
    emptyASTTS = ASTTS M.empty M.empty defFuns M.empty [0] 1 (toInteger (M.size defFuns + 1))
    go = do
      ptrAx <- mapM traverseAxiom pAx
      ptrAst <- mapM traverseDefinition pAst
      return (ptrAst,ptrAx)
    traverseDefinition (FunD (FunDef (FunClause fa ps t) expr)) = do
      pushScope
      fa' <- insertFun fa
      ps' <- mapM insertParam ps
      expr' <- traverseExpr expr 
      popScope
      return (FunD (FunDef (FunClause fa' ps' t) expr'))
    traverseDefinition (ProcD (ProcDef pd ps a)) = do
      pushScope
      ps' <- mapM insertParam ps
      a' <- traverseAST a
      popScope
      return (ProcD (ProcDef pd ps' a'))
    traverseAST (Assert asrt) = do
      asrt' <- traverseExpr asrt
      return (Assert asrt')
    traverseAST (Assume asum) = do
      asum' <- traverseExpr asum
      return (Assume asum')
    traverseAST (If a b c) = do 
      a' <- traverseExpr a
      b' <- traverseAST b
      c' <- traverseAST c
      return (If a' b' c')
    traverseAST (Block xs) = liftM Block $ mapM traverseAST xs
    traverseAST (Let (VarDecl m v e) xs) = do
      pushScope
      v' <- insertVar v
      e' <- traverseExpr e
      xs' <- mapM traverseAST xs
      popScope
      return (Let (VarDecl m v' e') xs')
     
    traverseAST (Assig (Assign v expr)) = do
      v' <- getVar v
      expr' <- traverseExpr expr
      return (Assig (Assign v' expr'))
    traverseExpr Undefined = return Undefined
    traverseExpr (V v) = liftM V $ getVar v
    traverseExpr (Eq a b) = do
      a' <- traverseExpr a
      b' <- traverseExpr b
      return (Eq a' b')
    traverseExpr (Ite a b c) = do
      a' <- traverseExpr a
      b' <- traverseExpr b
      c' <- traverseExpr c
      return (Ite a' b' c')
    traverseExpr (Apply f expr) = do
      f' <- insertFun f
      expr' <- mapM traverseExpr expr
      return (Apply f' expr')
    traverseAxiom (Forall vs expr) = do
      vs' <- mapM getVar vs
      expr' <- traverseExpr expr
      return (Forall vs' expr')
    insertParam (Param m v t) = do
      v' <- insertVar v
      return (Param m v' t)
    lookupVar v = do
      ss <- gets currentScopes
      vs <- gets insertedVars
      let candidates = mapMaybe (\s -> M.lookup (v,s) vs) ss 
      return
        (if null candidates
          then Nothing
          else Just (head candidates))

    insertVar v = do
      n <- gets nextId
      vm <- gets varMap
      si  <- gets (head . currentScopes)
      modify (\s -> s{varMap = M.insert (Var n) v vm,
                      insertedVars = M.insert (v,si) (Var n) (insertedVars s),
                      nextId = n + 1})
      return (Var n)

    getVar v = do
      mayV <-  lookupVar v
      case mayV of
        (Just v') -> return v'
        Nothing -> insertVar v

    pushScope = do
      n <- gets nextScope
      modify (\s -> s{
        currentScopes = n:currentScopes s,
        nextScope = n+1})
    popScope =
      modify (\s -> s{
        currentScopes = tail (currentScopes s)})
      
          
    insertFun f = do
      b <- gets (M.member f . insertedFuns)
      if b 
        then
          gets ((M.! f) . insertedFuns)
        else do
          n <- gets nextId
          fm <- gets funMap
          modify (\s -> s{funMap = M.insert (Fun n) f fm,
                          insertedFuns = M.insert f (Fun n) (insertedFuns s),
                          nextId = n + 1})
          return (Fun n)
