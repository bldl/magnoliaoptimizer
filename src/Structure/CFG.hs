{-@ LANGUAGE NoMonomorphismRestriction #-}

{- | The 'Structure.CFG' module defines the Control Flow
     Graph ('CFG') Structure which maintains a graph
      'CFGElem' nodes. These nodes contain the information about
      neighbours in the graph, a series of assignments and
      condition which dictates what child should be followed
      in during evaluation.

      The graphs generated from the Input language are Directed
      Asyclical Graphs (DAG). This property is maintained by
      all transformations.
-}
module Structure.CFG (
    Condition,
    IMap,
    CFGElem(..),
    getChildren,
    CFGPtr,
    Cfg(..),
    element,
    trueChild,
    falseChild,
    statements,
    parents,
    assignments,
    filterAssignments,
    vertecies,
    condition,
    variables,
    varAnnot,
    funAnnot,
    CFG,
    ReverseCFG(..),
    astToCFG,
    varAssignments)
  where

import qualified Data.Map as M
import           Control.Monad.State
import           Data.List


import           Structure.AST
import           Structure.DAG

type IMap = M.Map Integer

type Condition = Expression

{- | a CFGElem is a node in a CFG. -}
data CFGElem = CFGE{
  cfgParents :: [CFGPtr], -- ^ The incoming edges.
  cfgStatements :: [Statement], -- ^ The series of statements.
  cfgCondition :: Condition, -- ^ Boolean expression that dictates
                             -- which child chould be followed.
  cfgTrueChild :: CFGPtr, -- ^ The child that should followed when the
                       --   condition evaluates to True.
  cfgFalseChild :: CFGPtr  -- ^ The child that should be followed when the
                       --   condition evaluates to false.
  } deriving (Eq,Ord,Show)

-- | Returns the endpoint of the outgoing edges from
--   the Node.
getChildren :: CFGElem -> [CFGPtr]
getChildren (CFGE _ _ _ a b) = nub (filter (/= 0) [a,b] )

type CFGPtr = Integer

type CFG = Cfg ASTVarAnon ASTFunAnon ()

{- | A 'Cfg' represents a CFG and is parametriced by
     how variables and functions are annoted and what
     phi functions are inserted. -}
data Cfg v f phi = CFG{
  cfgRoot :: CFGPtr,
  cfgEnd :: CFGPtr,
  cfgVertices :: IMap CFGElem,
  cfgNumNode :: Integer,
  cfgPhiFunctions :: phi,
  cfgVarAnons :: M.Map Var v,
  cfgFunAnons :: M.Map Fun f,
  cfgDefintion :: ProcDef Var Fun,
  cfgOutVars :: [Param Var],
  cfgNextId :: Integer
  } deriving (Eq,Ord,Show)

{- | Returns the variable annotations associated with the
     variable. -}
varAnnot :: Var -> Cfg  v f phi -> v
varAnnot v cfg =
  case M.lookup v (cfgVarAnons cfg) of
    Nothing -> error "Internal Error: Unknown var pointer."
    (Just a) -> a

{- | Returns the function annotations associated with the
     function. -}
funAnnot :: Fun -> Cfg  v f phi -> f
funAnnot f cfg =
  case M.lookup f (cfgFunAnons cfg) of
    Nothing -> error "Internal Error: Unknown var pointer."
    (Just a) -> a

{- | Returns the assignments made on the vertex. -}
assignments :: Vertex -> Cfg v f phi -> [Assignment]
assignments v cfg =
  case M.lookup v (cfgVertices cfg) of
    Nothing -> error "Internal Error: Unknown vertex."
    (Just a) -> filterAssignments (cfgStatements a)


{- | Returns the statements made on the vertex. -}
statements :: Vertex -> Cfg v f phi -> [Statement]
statements v cfg =
  case M.lookup v (cfgVertices cfg) of
    Nothing -> error "Internal Error: Unknown vertex."
    (Just a) -> cfgStatements a

{- | Returns the parents of the given node. -}
parents :: Vertex -> Cfg v f phi -> [Vertex]
parents v cfg =
  case M.lookup v (cfgVertices cfg) of
    Nothing -> error "Internal Error: Unknown vertex."
    (Just a) -> cfgParents a

filterAssignments :: [Statement] -> [Assignment]
filterAssignments =
  concatMap (\x ->
          case x of 
            StAssign a -> [a]
            _          -> [])

{- | Returns the Element data for the given Vertex.-}
element :: Vertex -> Cfg v f phi -> CFGElem
element v cfg =
  case M.lookup v (cfgVertices cfg) of
    Nothing -> error "Internal Error: Unknown vertex."
    (Just a) -> a

{- | Returns the condition deciding which child is followed
     from the vertex.-}
condition :: Vertex -> Cfg v f phi -> Condition
condition v cfg =
  case M.lookup v (cfgVertices cfg) of
    Nothing -> error "Internal Error: Unknown vertex."
    (Just a) -> cfgCondition a

{- | Returns the child of the vertex followed if its
     condition hold.-}
trueChild :: Vertex -> Cfg v f phi -> Vertex
trueChild v cfg =
  case M.lookup v (cfgVertices cfg) of
    Nothing -> error "Internal Error: Unknown vertex."
    (Just a) -> cfgTrueChild a

{- | Returns the child of the vertex followed if its
     condition does not hold.-}
falseChild :: Vertex -> Cfg v f phi -> Vertex
falseChild v cfg =
  case M.lookup v (cfgVertices cfg) of
    Nothing -> error "Internal Error: Unknown vertex."
    (Just a) -> cfgFalseChild a


{- | The list of all vertecies. -}
vertecies :: Cfg v f phi -> [Vertex]
vertecies cfg = M.keys (cfgVertices cfg)

{- | The list of all variables. -}
variables :: Cfg v f phi -> [Var]
variables cfg = M.keys (cfgVarAnons cfg)

{- | A 'ReverseCFG' is a 'CFG' where all
  all edges swap direction. -}
data ReverseCFG a b c = ReverseCFG (Cfg a b c)

instance DAG (ReverseCFG a b c) where
  children r (ReverseCFG cfg) = 
    case M.lookup r (cfgVertices cfg) of
      Just ele -> filter (/=0) $ cfgParents ele
      Nothing   -> error "Unknown vertex in DAG"
  numNodes (ReverseCFG cfg) = cfgNumNode cfg
  root (ReverseCFG cfg) = cfgEnd cfg

instance DAG (Cfg a b c) where
  children r cfg = 
    case M.lookup r (cfgVertices cfg) of
      (Just ele) -> getChildren ele
      Nothing     -> error "Unknown vertex in DAG"
  numNodes cfg = cfgNumNode cfg
  root cfg = cfgRoot cfg



{- | 'varAssignments' cfg v returns a list of the indecies
     of all nodes where v is assigned to. -}
varAssignments :: Cfg a b () -> Var -> [CFGPtr]
varAssignments cfg v =
  concatMap (\(ptr,CFGE _ vs _ _ _) ->
    if vs `containAssignment` v 
      then [ptr]
      else [])
  . M.toList
  . cfgVertices 
  $ cfg
  where
    containAssignment vs v1 = any (\(Assign v2 _) -> v1 == v2) (filterAssignments vs)

data CFGTransformState = CFGTS{
   startNode :: CFGPtr,
   endNode :: CFGPtr,
   stateLookup :: IMap CFGElem,
   currentElem :: CFGElem,
   currentPtr :: CFGPtr,
   newPtr :: CFGPtr} deriving (Eq,Ord,Show)

endCFGE :: CFGPtr -> CFGElem
endCFGE e = CFGE [] [] trueFun e e


{- | 'astToCFG' transforms an AST to a CFG. -}
astToCFG :: ProcDef Var Fun -> M.Map Fun f -> M.Map Var v -> Integer -> Cfg v f ()
astToCFG pd@(ProcDef _ ps ast) fm vm nextId =
  CFG 
   (startNode s)
   (endNode s)
   (stateLookup s)
   (newPtr s)
   ()
   vm
   fm
   pd
   ps
   nextId
  where
   s = execState go startState
   startState = CFGTS 1 2 (M.fromList [(2,endCFGE 0)]) (endCFGE 2) 1 3
   go = do 
     transform ast
     end <- gets endNode
     curr <- gets currentPtr
     withCfg end (\c -> c{cfgParents = curr : cfgParents c})
     insertCurrent


newNode :: State CFGTransformState CFGPtr
newNode = do
  n <- gets newPtr
  modify (\s -> s{stateLookup = M.insert n (endCFGE (endNode s)) . stateLookup $ s,
                  newPtr = n + 1})
  return n

insertCurrent :: State CFGTransformState CFGPtr
insertCurrent = do
  c <- gets currentElem
  n <- gets currentPtr
  modify (\s -> s{stateLookup = M.insert n c . stateLookup $ s,
                  currentElem = endCFGE (endNode s),      
                  currentPtr = newPtr s,
                  newPtr = newPtr s + 1})
  return n


setCurrent :: CFGPtr -> State CFGTransformState ()
setCurrent ptr =
  modify (\s -> s{currentElem = stateLookup s M.! ptr,
                  currentPtr = ptr})



withCfg :: CFGPtr -> (CFGElem -> CFGElem) -> State CFGTransformState ()
withCfg cfgPtr f = 
  modify (\s -> s{stateLookup = M.adjust f cfgPtr . stateLookup $ s})


addParent :: Integer -> CFGPtr -> State CFGTransformState ()
addParent n p = withCfg n (\c -> c{cfgParents = p : cfgParents c})

transform :: AST -> State CFGTransformState ()
transform (Assig a) = 
  modify (\s -> s{currentElem = (currentElem s){cfgStatements = cfgStatements (currentElem s) ++ [StAssign a]}})
transform (Assert a) = 
  modify (\s -> s{currentElem = (currentElem s){cfgStatements = cfgStatements (currentElem s) ++ [StAssert a]}})
transform (Assume a) = 
  modify (\s -> s{currentElem = (currentElem s){cfgStatements = cfgStatements (currentElem s) ++ [StAssume a]}})

transform (Block asts) = do
    mapM_ transform asts

transform (Let (VarDecl _ _ Undefined) asts) = mapM_ transform asts
transform (Let (VarDecl _ v e) asts) = mapM_ transform (Assig (Assign v e):asts)

transform (If cond ast1 ast2) = do
  newEnd <- newNode
  oldEnd <- gets endNode
  modify (\s -> s{endNode = newEnd})
  this <- insertCurrent
  child1Start <- gets currentPtr
  transform ast1
  child1End <- insertCurrent
  child2Start <- gets currentPtr
  transform ast2
  child2End <- insertCurrent
  addParent child1Start this
  addParent child2Start this
  withCfg this (\c -> c{cfgTrueChild = child1Start,cfgFalseChild = child2Start,cfgCondition = cond})
  withCfg newEnd (\c -> c{cfgParents = child1End : child2End : cfgParents c})
  setCurrent newEnd
  modify (\s -> s{endNode = oldEnd})
