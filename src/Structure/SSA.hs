{- | The 'Structure.SSA' module defines
    an Single Static Assignment (SSA) CFG which
    where every variable only has one assignment.
    In order to accomodate derived variables, every 
    variable with extra information. -}
module Structure.SSA(
  SSA,
  SSAVA(..),
  SSAFA(..),
  SSAVarAnon,
  SSAFunAnon,
  PhiAssign(..),
  cfgToSSA,
  getPhiAxioms,
  phiFunctions,
  unrollCompoundExprs
  ) where

import qualified Data.Map as M
import qualified Data.Array as A
import qualified Data.Set as S
import           Control.Monad.State
import           Data.Maybe
import           Data.List

import Structure.CFG
import Structure.DAG
import Structure.AST
import Algorithm.DominatingFrontier

{- | 'PhiAssign' is an assignment to
     a result from a phi function. -}
data PhiAssign = PhiAssign {
  phiAssignVar :: Var, -- ^ The variable assigned to.
  phiId :: Integer, -- ^ The id of the phi function.
  joinVars :: [Var] -- ^ The parameters to the phi function. 
  } deriving (Eq,Ord,Show)

type SSAVarAnon = SSAVA ASTVarAnon

{- | Every Node of a SSA is annotated with additional
     information about which variable it is derived from
     (if any). -}
data SSAVA var = SSAVarAnon {
   origVar  :: var, -- ^ The variable it is derived from
                    -- ^ if it is derived, otherwise the variable itself.
   origVarIndex :: Maybe Var -- ^ Nothing if the variable is not a derived variable.
   } deriving (Eq,Ord,Show)

type SSAFunAnon = SSAFA ASTFunAnon
data SSAFA fun = SSAFun fun | PhiFun Var deriving (Eq,Ord,Show)

type SSA = Cfg SSAVarAnon SSAFunAnon (M.Map CFGPtr [PhiAssign])

addPhiAnnotations :: (Integer,M.Map Var (SSAVA a)) -> [Var] -> ((Integer,M.Map Var (SSAVA a)),[PhiAssign])
addPhiAnnotations (nId,anotMap) vs = ((newNext,newAnots),phiAssigs)
  where 
    (newNext,newAnots,phiAssigs) = foldl (\(n,m,as) v -> 
      let (SSAVarAnon vAnon _) = m M.! v in
      let varn = Var n in
      let varAnon = SSAVarAnon vAnon (Just v) in
        (n+2, M.insert varn varAnon m,PhiAssign varn (n+1) []:as)) (nId,anotMap,[]) vs

{- | Transforms a CFG into an SSA. -}
cfgToSSA :: Cfg a b () -> Cfg (SSAVA a) (SSAFA b) (M.Map CFGPtr [PhiAssign])
cfgToSSA cfg =
  let (varJoins,domSuccs) = iteratedDominatingFrontier cfg in
  let vMap = cfgVarAnons cfg in
  let transASTAnon = M.map (\a -> SSAVarAnon a Nothing) vMap in
  let ((newId,ssaVarAnons), phis) = M.mapAccum addPhiAnnotations (cfgNextId cfg,transASTAnon) varJoins in
  let phiFuns = map (\(PhiAssign v i _) -> (Fun i,PhiFun (getOrigVar ssaVarAnons v))) . concat . M.elems $ phis in
  let initVars = M.mapWithKey (\k _ -> [k]) vMap in
  let initState = CTSSAS newId (cfgVertices cfg) ssaVarAnons initVars phis [] in
  let ctssas = execState (replaceUses domSuccs (root cfg)) initState  in
  (CFG (root cfg) 
       (cfgEnd cfg) 
       (elemMap ctssas) 
       (cfgNumNode cfg) 
       (M.map (map (\p -> p{joinVars = nub (joinVars p)})) (phiMap ctssas))
       (varAnons ctssas)
       (M.map SSAFun (cfgFunAnons cfg) `M.union` M.fromList phiFuns)
       (cfgDefintion cfg)
       (map (replaceParam (varStacks ctssas)) (cfgOutVars cfg))
       (nextId ctssas))

replaceParam :: Ord v => M.Map v [v] -> Param v -> Param v
replaceParam m p@(Param{pvar = v}) = p{pvar = replaceVar m v}

{- | Returns the origin variable
 -   of the given variable if the variable is a
 -   derived variable. Otherwise the variable itself.-}
getOrigVar :: M.Map Var (SSAVA v) -> Var -> Var
getOrigVar sv v = 
  case M.lookup v sv of
   Nothing -> error "Internal error: Wrong lookup of origin variable."
   (Just (SSAVarAnon _ Nothing) ) -> v
   (Just (SSAVarAnon _ (Just a)) ) -> getOrigVar sv a

data CfgToSSAState v = CTSSAS {
  nextId :: Integer,
  elemMap :: M.Map CFGPtr CFGElem,
  varAnons :: M.Map Var (SSAVA v),
  varStacks :: M.Map Var [Var],
  phiMap :: M.Map CFGPtr [PhiAssign],
  pushedVars :: [[Var]]
  } deriving (Eq,Ord,Show)

{- Returns the phifunctions in the cfg node pointed to by the given
 - CFGPtr. -}
getPhiFuncs :: CFGPtr -> State (CfgToSSAState v) [PhiAssign]
getPhiFuncs i = gets (fromMaybe [] . (M.lookup i) . phiMap)

{- Traverses the underlying SSA in the state and replaces all occurences
 - of a reassignment with a fresh variable, and replaces uses. -} 
replaceUses :: A.Array CFGPtr (S.Set CFGPtr) -> CFGPtr -> State (CfgToSSAState a) ()
replaceUses domSuccs r = do
  newVarStack
  ele <- gets ((M.! r) . elemMap)
  let statemnts = cfgStatements ele
  phiFuncs <- getPhiFuncs r
  mapM_ pushPhiAssigns phiFuncs
  newAssigns <- mapM replaceUseAssignment statemnts
  vsts <- gets varStacks
  let newElem = ele{cfgStatements = newAssigns,
                     cfgCondition = replaceVarsExpr vsts (cfgCondition ele)}
  modify (\s -> s{elemMap = M.insert r newElem (elemMap s)})
  mapM_ addToPhiFuncs (getChildren ele)
  mapM_ (replaceUses domSuccs) (filter (/= 0) $ S.toList (domSuccs A.! r))
  popVars


{- | Returns the list of phi function joins associated with the vertex. -}
phiFunctions :: Vertex -> Cfg var fun (M.Map CFGPtr [PhiAssign]) -> [PhiAssign]
phiFunctions r cfg =
  case M.lookup r (cfgPhiFunctions cfg) of
    Nothing -> []
    (Just a) -> a

lookupOrigVar :: Var -> State (CfgToSSAState a) Var
lookupOrigVar v =
  liftM (fromMaybe v) $ gets (origVarIndex . (M.! v) . varAnons)

pushPhiAssigns :: PhiAssign -> State (CfgToSSAState a) ()
pushPhiAssigns (PhiAssign v _ _) = do
  oVar <- lookupOrigVar v
  pushVar oVar v

addToPhiFuncs :: CFGPtr -> State (CfgToSSAState a) ()
addToPhiFuncs y = do
  phiFuncs <- getPhiFuncs y

  newPhiFuncs <- forM phiFuncs (\(PhiAssign v i vs) -> do
    oVar <- lookupOrigVar v
    varStack <- gets ((M.lookup oVar) . varStacks)
    case varStack of
      Nothing   -> return (PhiAssign v i vs)
      (Just []) -> return (PhiAssign v i vs)
      Just (x:_) -> return (PhiAssign v i (x:vs)))

  modify (\s -> s{phiMap = M.insert y newPhiFuncs (phiMap s)})
  
fresh :: State (CfgToSSAState a) Integer
fresh = do 
  n <- gets nextId
  modify (\s -> s{nextId = n + 1})
  return n

pushVar :: Var -> Var -> State (CfgToSSAState a) ()
pushVar v vn = do
  vs <- gets varStacks
  modify (\s -> s{
    varStacks = M.alter (putOnTop vn) v vs,
    pushedVars = addToTop v (pushedVars s)})
  where
   {- add a variable to the top of the current stack.-}
   addToTop var [] = [[var]]
   addToTop var (x:xs) = (var:x):xs
   putOnTop var Nothing = Just [var]
   putOnTop var (Just vs) = Just (var:vs)

newVarStack :: State (CfgToSSAState v) ()
newVarStack = modify (\s -> s{pushedVars = []:(pushedVars s)})

popVars :: State (CfgToSSAState a) ()
popVars = do
  vs <- gets varStacks
  pvs <- gets pushedVars
  case pvs of
    [] -> return ()
    (xs:xss) -> modify (\s -> s{varStacks = foldl (\m x -> M.alter rmTop x m) vs xs,
                                pushedVars = xss})
  where
   {- Remove the top variable from the stack.-}
   rmTop Nothing = Nothing
   rmTop (Just []) = Nothing
   rmTop (Just [_]) = Nothing
   rmTop (Just (_:as)) = Just as



{-| Traverses the SSA and introduces new variables for every proper
subexpression. Eg a := f(g(x)) is changed to {subExpr_gx := g(x); a:=
f(subExpr_gx)}. -}
unrollCompoundExprs :: SSA -> SSA
unrollCompoundExprs cfg = execState (go (root cfg)) cfg
  where
    unrollExpr (V v2) =
      return (V v2,[])
    unrollExpr Undefined = return (Undefined,[])
    unrollExpr (Eq a b) = do
      (a',ass1)<- unrollExpr a
      (b',ass2)<- unrollExpr b
      return (Eq a' b',ass1 ++ ass2)
    unrollExpr (Ite a b c) = do
      (a',ass1)<- unrollExpr a
      (b',ass2)<- unrollExpr b
      (c',ass3)<- unrollExpr c
      return (Ite a' b' c',ass1 ++ ass2 ++ ass3)
    unrollExpr (Apply f []) =
      return (Apply f [],[])
    unrollExpr (Apply f exprs) = do
      xs <- mapM unrollExpr exprs
      let (vs,assigs) = unzip xs
      n <- gets cfgNextId
      (SSAFun origF) <- gets ((M.! f) . cfgFunAnons)
      let newVar = Var n
      let newAnon = SSAVarAnon (ASTVarAnon False ("subexpr_" ++ show (Apply f exprs)) (funType origF)) Nothing
      modify (\s ->
        s{cfgNextId = n + 1,
          cfgVarAnons = M.insert newVar newAnon (cfgVarAnons s)})
      return (V newVar,StAssign (Assign newVar (Apply f vs)) : concat assigs)
      
    unrollAssig (StAssign (Assign v1 (V v2))) = return [StAssign (Assign v1 (V v2))]
    unrollAssig (StAssign (Assign v1 (Apply f []))) = return [StAssign (Assign v1 (Apply f []))]
    unrollAssig (StAssign (Assign v (Apply f exprs))) = do
      xs <- mapM unrollExpr exprs
      let (vs,assigs) = unzip xs
      return (concat assigs ++ [StAssign (Assign v (Apply f vs))])
    unrollAssig (StAssign (Assign v (Eq e1 e2))) = do
      (e1',ass1) <- unrollExpr e1
      (e2',ass2) <- unrollExpr e2
      return (ass1 ++ ass2 ++ [StAssign (Assign v (Eq e1' e2'))])
    unrollAssig (StAssign (Assign v (Ite e1 e2 e3))) = do
      (e1',ass1) <- unrollExpr e1
      (e2',ass2) <- unrollExpr e2
      (e3',ass3) <- unrollExpr e3
      return (ass1 ++ ass2 ++ ass3 ++ [StAssign (Assign v (Ite e1' e2' e3'))])
    unrollAssig a = return [a]
    go 0 = return ()
    go r = do
      ele <- liftM (M.! r) (gets cfgVertices)
      cs <- mapM unrollAssig (cfgStatements ele)
      modify (\s -> s{cfgVertices = M.insert r ele{cfgStatements = concat cs} (cfgVertices s)})
      mapM_ go . children r =<< get

{- | Replaces the all uses of a variable in a statement by the variable currently on top
 - of the varstack. An assignment to an already assigned variable is replaced by a fresh
 - variable. -}
replaceUseAssignment :: Statement -> State (CfgToSSAState a) Statement
replaceUseAssignment (StAssign (Assign v exprs)) = do
  vn <- liftM Var fresh
  vAnon <- liftM (M.! v) $ gets varAnons
  vsts <- gets varStacks
  let vnAnnon = SSAVarAnon (origVar vAnon) (Just v)
  let retAssign = (Assign vn (replaceVarsExpr vsts exprs))
  modify (\s -> s{
    varAnons  = M.insert vn vnAnnon (varAnons s)})
  pushVar v vn
  return (StAssign retAssign)
replaceUseAssignment (StAssert e) = do
 vsts <- gets varStacks
 return (StAssert (replaceVarsExpr vsts e))
replaceUseAssignment (StAssume e) = do
 vsts <- gets varStacks
 return (StAssume (replaceVarsExpr vsts e))

replaceVar :: Ord v => M.Map v [v] -> v -> v
replaceVar m v =
  case M.lookup v m of
    Nothing -> v
    (Just []) -> v
    (Just (x:_)) -> x

{- | replaces all occurences of a given variable in an expression by the
 - variable currently on top of the varstack.-}
replaceVarsExpr :: Ord v => M.Map v [v] -> Expr v f -> Expr v f
replaceVarsExpr m (V v) = V $ replaceVar m v
replaceVarsExpr m (Apply f es) = Apply f (map (replaceVarsExpr m) es)
replaceVarsExpr m (Ite a b c) = Ite (replaceVarsExpr m a) (replaceVarsExpr m b) (replaceVarsExpr m c)
replaceVarsExpr m (Eq a b) = Eq (replaceVarsExpr m a) (replaceVarsExpr m b)
replaceVarsExpr _ Undefined  = Undefined

{- Returns a map from every variable to the block it has been
   assigned in -}
ssaAssignmentMap :: SSA -> M.Map Var CFGPtr
ssaAssignmentMap ssa = execState (go (root ssa)) M.empty
  where
   go r = do
     mapM_ (insertFromAssignment r) (assignments r ssa)
     let phis = phiFunctions r ssa
     mapM_ (insertFromPhis r) phis
     mapM_ go (children r ssa)
   insertFromAssignment r (Assign v _) = modify (M.insert v r)
   insertFromPhis r (PhiAssign v _ _) = modify (M.insert v r)


{- | Returns the axioms associated with the phifunctions in the SSA -}
getPhiAxioms :: SSA -> [Axiom]
getPhiAxioms ssa =
  map (go . checkPhi) . concat . M.elems . cfgPhiFunctions $ ssa
  where 
    go (PhiAssign _ i ps) = Forall [] (nestIfs (Apply (Fun i) (map V ps)) ps )
    nestIfs _   []     = error "Internal Error: Unjoined variable."
    nestIfs rhs [p]    = Eq (V p) rhs
    nestIfs rhs (p:ps) =
      case M.lookup p assigMap of
         Nothing    -> nestIfs rhs (ps ++ [p])
         (Just ptr) -> Ite (reachCond M.! ptr) (Eq (V p) rhs) (nestIfs rhs ps)
    -- Checks that the phifunction only has one
    -- variable that is missing from the assigMap 
    -- (the original variable defined before the root node)
    checkPhi phi@(PhiAssign _ _ ps) = 
      if length (filter (\p -> M.notMember p assigMap) ps) > 1
      then error ("Internal Error: Unassigned ssa variables: " ++ show ps)
      else phi
    assigMap = ssaAssignmentMap ssa
    reachCond = reachabilityCondition ssa
