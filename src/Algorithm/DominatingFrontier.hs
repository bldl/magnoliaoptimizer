
{- | Algorithms involving the computation of 
     the dominating frontier of  a 'CFG'. -}

module Algorithm.DominatingFrontier(
  iteratedDominatingFrontier,
  reachabilityCondition) where

import qualified Data.Set as S
import qualified Data.Map as M
import           Control.Monad
import           Control.Monad.ST.Safe
import           Data.Array.ST
import           Data.Array

import Algorithm.Idom
import Structure.DAG
import Structure.CFG
import Structure.AST

{- | Performs the 'iteratedDominatingFrontier' algorithm
     from \"Efficiently computing static single assignment form\"
     Cytron et al. 1991 -}
iteratedDominatingFrontier ::
   Cfg a b () -- ^ The input control flow graph
  -> (M.Map CFGPtr [Var],Array Integer (S.Set Integer)) -- ^ A pair consisting of map from cfg nodes to list of
                                                        -- variables that need joining and the dom-successors of
                                                        -- each CFGNode.
iteratedDominatingFrontier cfg = (varJoins,domSuccs)
  where
    varJoins = insertAll $
       foldl (\phis v -> 
         let varOccs = varAssignments cfg v in 
         go S.empty (S.fromList varOccs) phis [] varOccs v)
       [] (variables cfg)
    (df,domSuccs) = runST (dominatingFrontier cfg)
    insertAll = foldl (\m (ptr,var) -> M.alter (Just . maybe [var] (var:)) ptr m) M.empty
    go _ _ phis [] [] _  = phis
    go hasAlready work phis [] (x:xs) v = go hasAlready work phis (S.toList (df ! x)) xs v
    go hasAlready work phis (y:ys) xs v = 
      if y `S.notMember` hasAlready then
        if y `S.notMember` work then
          go (S.insert y hasAlready) (S.insert y work) ((y,v):phis) ys (y:xs) v
        else
          go (S.insert y hasAlready) work ((y,v):phis) ys xs v
      else
        go hasAlready work phis ys xs v

{- | Calculates a conservative reachability condition for every
     node. Returns a map from nodes to conditions. This is due Cytron et. al.
     \"Efficiently computing static single assignment form and the contro dependence
     graph\" 1991. -}
reachabilityCondition :: Cfg a b c -> M.Map CFGPtr Condition
reachabilityCondition cfg = M.fromList .  map (\k -> (k,reaches k)) . vertecies $ cfg
  where
    reaches y 
      | y == root cfg    = trueFun
      | (dom ! y) ==  0  = falseFun
      | S.null (pdf ! y) = trueFun
      | otherwise        = foldl1 orFun
         (map (\x -> predicate y x `andFun` reaches x) (S.toList (pdf ! y)))
    predicate y x =
      if decendant y (trueChild x cfg)
        then condition x cfg
        else notFun (condition x cfg)
    decendant y x
      | y == x = True
      | otherwise = any (decendant y) (S.toList (pDomSuccs ! x))
    (dom,_,_,_,_) = runST (idomDfs cfg)
    (pdf,pDomSuccs) = runST (dominatingFrontier (ReverseCFG cfg))

{- | Calculates the dominatingFrontier for each node. If
     df = runST (dominatingFrontier dag) then df ! n is a set
     of the nodes in the dominatingFrontier of n. -}
dominatingFrontier :: (DAG a) => a -> ST s (Array Integer (S.Set Integer),Array Integer (S.Set Integer))
dominatingFrontier dag = do
  df <- ((newArray (0,numNodes dag) S.empty) :: ST s (STArray s Integer (S.Set Integer)))
  (dom,_,vertex,_,domSuccs) <- idomMDfs dag 
  forM_ [1..numNodes dag] (\i -> do
    x <- readArray vertex i
    unless (x == 0) $ do
      forM_ (children x dag) (\y -> do
        yDom <- readArray dom y
        xDf <- readArray df x
        when (yDom /= x)
          (writeArray df x (S.insert y xDf)))

      xSuccs <- readArray domSuccs x

      forM_ (S.toList xSuccs) (\z -> do
        zDf <- readArray df z
        forM_ (S.toList zDf) (\y -> do
          yDom <- readArray dom y
          when (yDom /= x) (do
            xDf <- readArray df x
            writeArray df x (S.insert y xDf)))))
  finalDomSuccs <- freeze domSuccs
  finalDf <- freeze df
  return (finalDf ,finalDomSuccs)
