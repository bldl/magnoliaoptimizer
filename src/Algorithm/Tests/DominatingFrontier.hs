module Algorithm.Tests.DominatingFrontier where

import qualified Data.Map as M

import           Algorithm.DominatingFrontier

import           Structure.Tests.CFG
import           Structure.CFG

testAllElemsContained :: ArithCFG -> Bool
testAllElemsContained cfg =
  let (varJoins,_) = iteratedDominatingFrontier cfg in
  all ((\x ->  M.member x (cfgVertices cfg)) . fst) (M.toList varJoins)

testAllVarsContained :: ArithCFG -> Bool
testAllVarsContained cfg =
  let (varJoins,_) = iteratedDominatingFrontier cfg in
  all (all (\x ->  M.member x (cfgVarAnons cfg)) . snd) (M.toList varJoins)
