module Algorithm.Tests.Idom where

import qualified Data.Map as M
import qualified Data.Array as A
import           Data.Maybe
import           Test.QuickCheck
import           Data.List
import           Data.Ord
import           Control.Monad.ST

import           Structure.DAG
import           Algorithm.Idom


type Path = [Vertex]

data MapDAG = MapDAG (M.Map Integer [Integer]) Integer deriving (Eq,Ord,Show)

instance DAG MapDAG where
  children r (MapDAG m _) = fromMaybe [] (M.lookup r m)
  root (MapDAG _ n) = if n == 0 then 0 else 1
  numNodes (MapDAG _ n) = n

instance Arbitrary MapDAG where
  shrink = shrinkNothing
  arbitrary = do
    n <- choose (0,50)
    xs <- mapM (\x -> listOf (choose (x+1,n))) [1..n-1]
    return (MapDAG (M.fromList . zip [1..] . map (nub . genericTake n) $ xs) n)
  


-- Lemma 2 of Lengauer et al. : for every reachable vertex other
-- than the root there is a nontrivial path from idom(w) to w in
-- the dfs tree.
existsPathFromIdom :: MapDAG -> Positive Vertex -> Property
existsPathFromIdom dag (Positive v') =
  let v = if numNodes dag == 0 then 0 else v' `mod` numNodes dag in
  let (idom,_,_,parent,_) = runST $ idomDfs dag in
  (v /= 0 && v /= 1) ==> 
    ((idom A.! v == 0) || (isAncestorOf parent (idom A.! v) v
    && v /= idom A.! v))

-- Lemma 3 of Lengauer et al. : for every reachable vertex other than the
-- root there is a nontrivial path from sdom(w) to w in the
-- dfs tree.
existsPathFromSdom :: MapDAG -> Positive Vertex -> Property
existsPathFromSdom dag (Positive v') =
  let v = if numNodes dag == 0 then 0 else v' `mod` numNodes dag in
  let (idom,sdom,vertex,parent,_) = runST $ idomDfs dag in
  (v /= 0 && v /= 1) ==> ( 
    let vsdom = vertex A.! (sdom A.! v) in
    (idom A.! v == 0) ||
      (isAncestorOf parent vsdom v &&  vsdom /= v))

-- Lemma 4 of Lengauer et al. : for every reachable vertex other than the
-- root there is a (possibly trivial) path from idom(w) to sdom(w) in the
-- dfs tree.
existsPathFromIdomToSdom :: MapDAG -> Positive Vertex -> Property
existsPathFromIdomToSdom dag (Positive v') =
  let v = if numNodes dag == 0 then 0 else v' `mod` numNodes dag in
  let (idom,sdom,vertex,parent,_) = runST $ idomDfs dag in
  (v /= 0 && v /= 1) ==> 
    ((idom A.! v == 0) || 
      isAncestorOf parent (idom A.! v) (vertex A.! (sdom A.! v)))

-- Lemma 5 of Lengauer et al. : for any vertices v,w where there is
-- a path from v to w in the DFS tree, there is a path from v to
-- idom(w) or a path from idom(w) to idom(v).
idomCoOnPaths :: MapDAG -> Positive Vertex -> Positive Vertex -> Property
idomCoOnPaths dag (Positive v') (Positive w') =
  let v = if numNodes dag == 0 then 0 else v' `mod` numNodes dag in
  let w = if numNodes dag == 0 then 0 else w' `mod` numNodes dag in
  let (idom,_,_,parent,_) = runST $ idomDfs dag in
  (v /= 0 && w /= 0) ==> 
    ((idom A.! v == 0) || not (isAncestorOf parent v w) || 
     (isAncestorOf parent v (idom A.! w) ||
      isAncestorOf parent (idom A.! w) (idom A.! v)))

-- Theorem 3 of Lengauer et al. : Let w be a reachable vertex /= root and let u be a vertex where
-- sdom(u) has minimum dfsNum among vertices where there is a path sdom(w) -> u -> w,
-- and sdom(w) /= u. Then dfsNum(sdom(u)) <= dfsNum(sdom(w)) and idom(u) = idom(w).
thm3Lengauer :: MapDAG -> Positive Integer -> Property
thm3Lengauer dag (Positive w') =
  let w = if numNodes dag == 0 then 0 else w' `mod` numNodes dag in
  let (idom,sdom,vertex,parent,_) = runST $ idomDfs dag in
  (w /= 0 && idom A.! w /= 0 && w /= 1) ==> 
    (let wSdom = vertex A.! (sdom A.! w) in
     let u = minimumBy (comparing (sdom A.!)) ( filter (\u' -> isAncestorOf parent u' w && isAncestorOf parent wSdom u' && wSdom /= u') [1.. numNodes dag -1]) in
     sdom A.! u <= sdom A.! w && idom A.! u == idom A.! w)


isAncestorOf :: A.Array Integer Integer -> Integer -> Integer -> Bool
isAncestorOf parent x v
  | x == v              = True
  | (parent A.! v) == 0 = False -- No more parents
  | otherwise           = isAncestorOf parent x (parent A.! v)
