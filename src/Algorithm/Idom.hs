{-# LANGUAGE ScopedTypeVariables #-}

{- | The 'Algorithm.Idom' module implements the idom algorithm
     from \"A fast algorithm for finding dominators in a flowgraph\"
     Lengauer et al. -}

module Algorithm.Idom(idomDfs,idomMDfs) where
import           Prelude hiding (pred)

import qualified Data.Set as S
import           Data.STRef
import           Control.Monad
import           Control.Monad.ST.Safe
import           Data.Array.ST
import           Data.Array


import Structure.DAG


{- | 'idomDfs' runs the idom
     algorithm and returns a touple of 
     arrays, describing the result. In
     (idom,sdom,dfsNum,parent,domSuccs) = runST (idomDfs dag), 
     idom ! v is the imediate dominator of v, sdom ! v is the
     semidominator of v, vertex ! v is the dfs ! v number of v,
     parent ! v is the parent of v in the dfs tree and domSuccs ! v
     is the successors of v in the dfs tree.-}
idomDfs :: DAG a =>
  a -> ST s (Array Integer Integer,
             Array Integer Integer,
             Array Integer Integer,
             Array Integer Integer,
             Array Integer (S.Set Integer))
idomDfs dag = do
  (maxNum,label,ancestor,semi,vertex,parent,domSuccs,dom,pred,bucket,dfsNext) <- idomInit dag
  dfs dag pred semi vertex parent (root dag) dfsNext
  _ <- idomCompute (maxNum,label,ancestor,semi,vertex,parent,domSuccs,dom,pred,bucket)
  retDom <- freeze dom
  retSdom <- freeze semi 
  retVertex <- freeze vertex
  retParent <- freeze parent
  retDomSuccs <- freeze domSuccs
  return (retDom,retSdom,retVertex,retParent,retDomSuccs)

-- | 'idomMDfs' is a equivalent to 'idomDfs' but it returns mutable arrays.
idomMDfs :: DAG a =>
  a -> ST s (STArray s Integer Integer,
             STArray s Integer Integer,
             STArray s Integer Integer,
             STArray s Integer Integer,
             STArray s Integer (S.Set Integer))
idomMDfs dag = do
  (maxNum,label,ancestor,semi,vertex,parent,domSuccs,dom,pred,bucket,dfsNext) <- idomInit dag
  dfs dag pred semi vertex parent (root dag) dfsNext
  _ <- idomCompute (maxNum,label,ancestor,semi,vertex,parent,domSuccs,dom,pred,bucket)
  return (dom,semi,vertex,parent,domSuccs)

dfs :: DAG a =>
     a
     -> STArray s Vertex (S.Set Vertex)
     -> STArray s Vertex Integer
     -> STArray s Integer Vertex
     -> STArray s Vertex Vertex
     -> Vertex
     -> STRef s Integer
     -> ST s ()
dfs _   _    _    _      _      0 _ = return ()
dfs dag pred semi vertex parent v dfsNext = do
  n <- readSTRef dfsNext
  writeSTRef dfsNext (n+1)
  writeArray semi v n
  writeArray vertex n v
  forM_ (children v dag) (\ w -> do
    wSemi <- readArray semi w
    when (wSemi == 0) (do 
      writeArray parent w v
      dfs dag pred semi vertex parent w dfsNext)
    wPred <- readArray pred w
    writeArray pred w (S.insert v wPred))


idomInit  :: (DAG a) =>
     a
     -> ST
          s
          (Integer,
           STArray s Integer Integer,
           STArray s Integer Integer,
           STArray s Integer Integer,
           STArray s Integer Integer,
           STArray s Integer Integer,
           STArray s Integer (S.Set Integer),
           STArray s Integer Integer,
           STArray s Integer (S.Set Integer),
           STArray s Integer (S.Set Integer),
           STRef s Integer)
idomInit dag = do
  let maxNum = numNodes dag
  label <-    ((newListArray (0,maxNum) [0..])      :: ST s (STArray s Integer Integer))
  ancestor <- ((newArray (0,maxNum) 0)              :: ST s (STArray s Integer Integer))
  semi <-     ((newArray (0,maxNum) 0)              :: ST s (STArray s Integer Integer))
  vertex <-   ((newArray (0,maxNum) 0)              :: ST s (STArray s Integer Integer))
  parent <-   ((newArray (0,maxNum) 0)              :: ST s (STArray s Integer Integer))
  dom <-      ((newArray (0,maxNum) 0)              :: ST s (STArray s Integer Integer))
  domSuccs <- ((newArray (0,maxNum) S.empty)        :: ST s (STArray  s Integer (S.Set Integer)))
  pred <-     ((newArray (0,maxNum) S.empty)        :: ST s (STArray  s Integer (S.Set Integer)))
  bucket <-   ((newArray (0,maxNum) S.empty)        :: ST s (STArray  s Integer (S.Set Integer)))
  dfsNext <- newSTRef 0
  return (maxNum,label,ancestor,semi,vertex,parent,domSuccs,dom,pred,bucket,dfsNext)



idomCompute ::
  (Integer,
   STArray s Integer Integer,
   STArray s Integer Integer,
   STArray s Integer Integer,
   STArray s Integer Integer ,
   STArray s Integer Integer,
   STArray s Integer (S.Set Integer),
   STArray s Integer Integer,
   STArray s Integer (S.Set Integer),
   STArray s Integer (S.Set Integer))
  -> ST s (STArray s Integer Integer, STArray s Integer (S.Set Integer))
idomCompute (maxNum,label,ancestor,semi,vertex,parent,domSuccs,dom,pred,bucket) = do
  forM_ [maxNum,maxNum-1..1] (\i -> do
    w <- readArray vertex i 
    wPred <- readArray pred w
    forM_ (S.toList wPred) (\v -> do
      u <- eval semi ancestor label v
      uSemi <- readArray semi u
      wSemi <- readArray semi w
      when (uSemi < wSemi)
        (writeArray semi w uSemi))
    wSemi <- readArray semi w
    wSemiVertex <- readArray vertex wSemi
    wBucket <- readArray bucket wSemiVertex
    writeArray bucket wSemiVertex (S.insert w wBucket)
    wParent <- readArray parent w
    link ancestor wParent w
    wBucketParent <- readArray bucket wParent
    forM_ (S.toList wBucketParent) (\v -> do
      u <- eval semi ancestor label v
      uSemi <- readArray semi u
      vSemi <- readArray semi v
      if uSemi < vSemi 
        then do 
          writeArray dom v u
          uDomSuccs <- readArray domSuccs u
          writeArray domSuccs u (S.insert v uDomSuccs)
        else do
          writeArray dom v wParent
          wParentDomSuccs <- readArray domSuccs wParent
          writeArray domSuccs wParent (S.insert v wParentDomSuccs))
    writeArray bucket wParent S.empty)
  forM_ [1..maxNum] (\i -> do
    w <- readArray vertex i
    wDom <- readArray dom w
    wSemi <- readArray semi w
    wVertexSemi <- readArray vertex wSemi
    when (wDom /= wVertexSemi) (do
      wDomDom <- readArray dom wDom
      writeArray dom w wDomDom
      wDomDomSuccs <- readArray domSuccs wDomDom
      writeArray domSuccs wDomDom (S.insert w wDomDomSuccs)))
  when (maxNum > 0) (do
    r <- readArray vertex 0
    writeArray dom r 0
    writeArray domSuccs 0 (S.singleton r))
  return (dom,domSuccs)


compress :: 
  STArray s Integer Integer ->
  STArray s Integer Integer ->
  STArray s Integer Integer ->
  Integer -> ST s ()
compress semi ancestor label v = do
  vA <- readArray ancestor v
  vAA <- readArray ancestor vA
  when (vAA /= 0) ( do
    compress semi ancestor label vA
    vAncestor <- readArray ancestor v
    vAncestorAncestor <- readArray ancestor vAncestor
    vLabelAncestor <- readArray label vAncestor
    vSemiLabelAncestor <- readArray semi vLabelAncestor
    vLabel <- readArray label v
    vSemiLabel <- readArray semi vLabel
    when (vSemiLabelAncestor < vSemiLabel)
      (writeArray label v vLabelAncestor)
    writeArray ancestor v vAncestorAncestor)
          
link :: 
  STArray s Integer Integer ->
  Integer ->
  Integer ->
  ST s ()
link ancestor v w = writeArray ancestor w v

eval ::
  STArray s Integer Integer ->
  STArray s Integer Integer ->
  STArray s Integer Integer ->
  Integer ->
  ST s Integer
eval semi ancestor label v = do
  vAncestor <- readArray ancestor v
  if vAncestor == 0 
    then
      return v
    else do
      compress semi ancestor label v
      readArray label v
