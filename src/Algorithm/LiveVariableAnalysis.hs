module Algorithm.LiveVariableAnalysis where

import Structure.AST
import Structure.CFG
import Structure.SSA
import Algorithm.DataFlowAnalysis

import           Control.Monad
import           Data.Maybe
import qualified Data.Map as M
import qualified Data.Set as S
import           Data.List


data LiveReason = 
  LiveFor Var 
  | CarriedWith Condition deriving(Eq,Ord,Show)
type LivenessTrace = [LiveReason]
data LivenessData = LivenessData{
 liveIn :: M.Map Var LivenessTrace,
 changed :: Bool} deriving (Show)


combineSent :: CFGElem -> [(CFGPtr,[Var])] -> [(LiveReason, Var)]
combineSent CFGE{
  cfgCondition = cond,
  cfgTrueChild = tc,
  cfgFalseChild = fc} lives = 
  concat . mapMaybe go $ lives
   where
     go (ptr,live)
       | ptr == tc && cond == falseFun = Nothing
       | ptr == fc && cond == trueFun  = Nothing
       | otherwise = Just (zip (repeat (CarriedWith cond)) live)

insertTrace :: LiveReason -> Var
  -> DataFlowAnalysis LivenessData [Var] (Maybe Var)
insertTrace x v = do
  mtr <- liftM (M.lookup v . liveIn) getNodeData
  case mtr of
    Nothing -> do
      updateNodeData (\d -> d{
        changed = True,
        liveIn = M.insert v [x] (liveIn d)})
      return (Just v)
    (Just xs) -> do
      updateNodeData (\d -> d{
        liveIn = M.insert v (x:xs) (liveIn d)})
      return Nothing

livenessAnalysis :: SSA -> M.Map CFGPtr LivenessData
livenessAnalysis cfg = 
  backwardsDFA
    (map pvar . cfgOutVars $ cfg)
    (LivenessData M.empty True) cfg go
  where
    deleteSnd _ [] = []
    deleteSnd y (x:xs)
      | y == snd x = xs
      | otherwise = x:deleteSnd y xs
    sndElem y = any (\x -> snd x == y)
    go el newLiveOut' = do
      let newLiveOut = combineSent el newLiveOut'
      updateNodeData (\d -> d{changed = False})
      (newLiveIn,carr) <- foldM (\(newLive,carried) st ->
        case st of
          (StAssume _) -> return (newLive,carried)
          (StAssert _) -> return (newLive,carried)
          (StAssign (Assign x e)) ->
            if x `elem` newLive || x `sndElem` carried
              then do
               let vars = S.toList (getVars e)
               inserted <- liftM catMaybes
                 $ mapM (insertTrace (LiveFor x)) vars
               return (inserted ++ delete x newLive, deleteSnd x carried)
              else
               return (newLive,carried)) ([],newLiveOut) (cfgStatements el)
      mapM_ (uncurry insertTrace) carr
      return (newLiveIn ++ map snd carr,True)
