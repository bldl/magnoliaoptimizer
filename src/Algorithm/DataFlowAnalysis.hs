{-# LANGUAGE GeneralizedNewtypeDeriving#-}
module Algorithm.DataFlowAnalysis(
  DataFlowAnalysis,
  updateNodeData,
  getNodeData,
  backwardsDFA,
  addToWorkList,
  sendTo)
   where

import Structure.CFG

import qualified Data.Map as M
import Control.Monad
import Control.Monad.State
import Control.Applicative
import Data.Maybe
import qualified Data.Sequence as S
import Data.Sequence(Seq,(><),ViewR(..),viewr)

data DataFlowState dat com = DataFlowState {
  currPtr     :: CFGPtr,
  nodeData    :: M.Map CFGPtr dat,
  defaultData :: dat,
  sendtTo     :: M.Map CFGPtr [(CFGPtr,com)],
  worklist    :: Seq CFGPtr} deriving (Eq,Ord,Show)


newtype DataFlowAnalysis a c b =
  DataFlowAnalysis {
    unDataFlow :: State (DataFlowState a c) b}
  deriving (Monad)

addToWorkList_ :: [CFGPtr] -> State (DataFlowState a c) ()
addToWorkList_ xs = 
  modify (\s -> s{worklist =  S.fromList xs >< worklist s})

addToWorkList :: [CFGPtr] -> DataFlowAnalysis a c ()
addToWorkList = DataFlowAnalysis . addToWorkList_

popWorkList :: State (DataFlowState a c ) (Maybe CFGPtr)
popWorkList = do
  ws <- gets worklist
  case viewr ws of
    EmptyR -> return Nothing
    (ws' :> a) -> do
      modify (\s -> s{worklist = ws'})
      return (Just a)

updateNodeData :: (a -> a) -> DataFlowAnalysis a c ()
updateNodeData f = DataFlowAnalysis $ do
  k <- gets currPtr
  modify (\s -> s{
    nodeData =  M.alter (\m -> f <$> (m <|> Just (defaultData s))) k (nodeData s)})

getNodeData :: DataFlowAnalysis a c  a
getNodeData = DataFlowAnalysis $ do
  k <- gets currPtr
  def <- gets defaultData
  gets (fromMaybe def. M.lookup k . nodeData)

sendTo :: CFGPtr -> (CFGPtr,c) -> DataFlowAnalysis a c ()
sendTo p c = DataFlowAnalysis $
  modify (\s -> s{
    sendtTo = M.alter (\m -> Just (c : fromMaybe [] m)) p (sendtTo s)})

doWork ::
  (CFGPtr -> State (DataFlowState a c ) ()) 
  -> State (DataFlowState a c )  () 
doWork f = go
  where 
  go =  do
  mx <- popWorkList
  case mx of
    Nothing -> return ()
    (Just x) -> do
      f x
      go

backwardsDFA ::
  c -> -- ^ initial data for the end node
  a -> -- ^ default data for all other nodes
  Cfg var fun phi -> -- ^ the CFG to run the DFA on
  (CFGElem ->
   [(CFGPtr,c)] ->
   DataFlowAnalysis a c (c,Bool)) ->
    -- ^ The DFA computation on every node
  M.Map CFGPtr a -- ^ The computed data for each node
backwardsDFA initData defData cfg f = nodeData $ execState (doWork (\x ->
  do
  s <- liftM (fromMaybe []) $ gets (M.lookup x . sendtTo)
  (send,changed) <- unDataFlow $ f (element x cfg) s
  let prnts = parents x cfg
  mapM_ (\p -> unDataFlow $ sendTo p (x,send)) prnts
  if changed
    then addToWorkList_ prnts
    else return ())) initState
  where
    end = cfgEnd cfg
    initState =
      DataFlowState
        end
        M.empty
        defData
        (M.singleton end [(end,initData)])
        (S.singleton end)

