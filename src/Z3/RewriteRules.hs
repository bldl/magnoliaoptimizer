module Z3.RewriteRules where
import Structure.AST

import qualified Data.Map as M

data RewriteSuggestion = RwSuggest{
  freeVariables :: [Var],
  expression :: Expression} deriving (Eq,Ord,Show)

instantiateSuggests :: M.Map Var ASTVarAnon -> [Var] -> RewriteSuggestion -> [Expression]
instantiateSuggests vAns uqs sugg = 
  let getCandidate fv = filter (\a -> varType (vAns M.! a) == varType (vAns M.! fv)) uqs in
  let candidateInsts = map getCandidate (freeVariables sugg) in
  map (foldl (\e (a,b) -> substitute a b e) (expression sugg) . zip (freeVariables sugg))
  (oneFromEach candidateInsts)

oneFromEach :: [[a]] -> [[a]]
oneFromEach [] = [[]]
oneFromEach (ys:xss) = [y:xs| y <- ys, xs <- oneFromEach xss] 

axiomToSuggestion :: Axiom -> [RewriteSuggestion]
axiomToSuggestion (Forall vs expr) = go expr
  where 
    go (V _)         = []
    go Undefined     = []
    go (Apply _ es)  = concatMap go es
    go (Ite _ e1 e2) = go e1 ++ go e2
    go (Eq e1 e2)    = RwSuggest vs e1 : RwSuggest vs e2 : (go e1 ++ go e2)
