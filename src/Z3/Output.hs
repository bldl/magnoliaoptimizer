module Z3.Output where

import Structure.SSA
import Structure.CFG
import Structure.AST

import qualified Data.Map as M
import Data.Map(Map)

-- | The state of the sat solver
data SATSTATE =
  SAT -- ^  SAT means that the current formula is satisfiable
  | UNSAT  -- ^ UNSAT means that the current formula is unsatisfiable
  | UNKNOWN -- ^ UNKNOWN can mean several things, the solver might not
            -- ^ be able to show either unsat or sat, or it might
            -- ^ have run out of system resources.
    deriving (Eq,Ord,Show)

type LineNumber = Int
type LinePointer = (CFGPtr,LineNumber)
type SsaId = Int

unList :: Int -> [String] -> String
unList _ [] = "[]"
unList _ [x] = "[" ++ x ++ "]"
unList i xs = "[" ++ unComma i xs ++ replicate i ' ' ++ "]"
unArgs :: String -> Int -> [String] -> String
unArgs n _ [] = n ++ "()"
unArgs n _ [x] = n ++ "(" ++ x ++ ")"
unArgs n i xs = n ++ "(" ++ unComma i xs ++ replicate i ' ' ++ ")"

unComma :: Int -> [String] -> String
unComma = go False
  where
    go _ _ [] = ""
    go t i ("":xs) = go t i xs
    go False i (x:xs) = x ++ "\n" ++ go True i xs
    go True  i (x:xs) = replicate i ' ' ++ "," ++ x ++ "\n" ++ go True i xs

showOutputs :: Map Var SSAVarAnon -> Map Fun SSAFunAnon -> [Z3Output] -> String
showOutputs _  _  [] = "SSA()"
showOutputs vm fm os = unArgs "SSA" 0 $ [
  showVarInfos vm 
  ,showFunInfos fm
  ,unComma 0
    (zipWith (\i o ->
     let p = procDecl . cfgDefintion  . outSsa $ o in
     unComma 0 [
      showBlocks i o
      ,showProcEntries i p o
      ,showEquivs i o
      ,showAssertInfos o]) [1..] os)
  ]

showProcEntries :: Int -> ProcDecl -> Z3Output -> String
showProcEntries i p o = unArgs "ProcEntry" 2
    [show i ++ "@" ++ (show . cfgRoot . outSsa $ o),
     show p]

showBlocks :: SsaId -> Z3Output -> String
showBlocks sId Z3Output{outSsa = s} =
  unComma 0 $ map (\(c,e) ->
   unArgs "BlockNode" 2
     [show sId ++ "@" ++ show c,
      unList 4 (map showPhi . getPhis . M.lookup c . cfgPhiFunctions $ s),
      unList 4 (map showStatement (cfgStatements e))]) (M.assocs . cfgVertices $ s)

showPhi :: PhiAssign -> String
showPhi (PhiAssign v i joins) =
      unArgs "PhiAssign" 6
        [show v, "Phi" ++ show i ++ "(" ++
         show joins ++ ")"]

getPhis :: Maybe [PhiAssign] -> [PhiAssign]
getPhis Nothing = []
getPhis (Just xs) = xs

showStatement :: Statement -> String
showStatement (StAssign a) = show a
showStatement (StAssert e) = "Assert(" ++ show e ++ ")"
showStatement (StAssume e) = "Assume(" ++ show e ++ ")"

showVarInfos :: Map Var SSAVarAnon -> String
showVarInfos va =
  unComma 0 $ map (\(v,vi) -> "VarInfo(" ++ show v ++"," ++ go vi ++ ")") . M.assocs $ va
   where
     go SSAVarAnon{origVar = o} = show o
    
showFunInfos :: Map Fun SSAFunAnon -> String
showFunInfos fa =
  unComma 0 $ map (\(f,fi) -> "FunInfo(" ++ show f ++"," ++ go fi ++ ")") . M.assocs $ fa
   where
     go (SSAFun f) = show f
     go (PhiFun v) = "Phi(" ++ show v ++ ")"

showEquivs :: SsaId -> Z3Output -> String
showEquivs si Z3Output{z3Equivs = es} = 
  unComma 0 $ map (\(Equiv v e (cp,ln)) -> 
    "Equiv(" ++ show v ++ "," ++ show e ++ "," ++ show si ++ "@" ++ show cp ++ ":" ++ show ln ++ ")")
    es

showAssertInfos :: Z3Output -> String
showAssertInfos Z3Output{assertInfo = ais} =
  unComma 0 $ map (\(e,l,s) ->
    "AssertInfo(" ++ show e ++ "," ++ show l ++ "," ++ show s ++ ")")
    ais

data Z3Output = Z3Output {
  outSsa :: SSA,
  z3Equivs :: [Equiv],
  assertInfo :: [(Expression,LinePointer,AssertState)]} deriving (Eq,Ord,Show)

data AssertState = Taut | Contr | Assumed | Unknown deriving (Eq,Ord,Show)

z3Output :: SSA -> [Equiv] -> [(Expression, LinePointer, AssertState)] -> Z3Output
z3Output = Z3Output

data Equiv = Equiv Expression Expression LinePointer deriving (Eq,Ord,Show)
